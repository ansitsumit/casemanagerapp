<?php

		   
		   //get email format..
	function get_email_format($toName, $message){
		 $empmsg  = '<div style="font-family:Arial, Helvetica, sans-serif; margin:0px; padding:0px; font-size:12px;">
		  <!--mail-Start-here-->
		<div style="width:800px; margin:0px auto; padding:5px;">

		<div style="clear:both;"></div>
		<p style="color:#5F5F5F; font-size:12px; line-height:20px; padding:0px 6px 6px 10px;"><strong>Dear '.$toName.',</strong>
		</p>
		<div style="width:770px; padding:10px;">
		<p style="font-size:14px; color:#5F5F5F;">
			'.($message).'
		</p>
		
		</div>
		<br/>

		<p style="color:#5F5F5F; font-size:12px; line-height:20px; padding:0px 8px 8px 12px;">Best Regards, <br/>
		<a href="http://www.casemanagementapp.com/" style="color:#cb202d; font-size:12px; text-decoration:underline;">www.casemanagementapp.com</a></p>
		<div style="padding:10px; float:left;"><a href="#"><img src="'.base_url().'media/images/logo.png" style="border:none;" /></a></div>
		
		</div>  
		<!--mail-end-here-->

		</div>';
		return $empmsg;
	}
	
	function  get_sequence_name($id)
	{
		 $ci=& get_instance();
         $ci->load->database();
		 $ci->db->select('SequenceName');
		 $ci->db->from('tblcasesequence');
		 $ci->db->where('CaseSequenceID',$id);
		 $query = $ci->db->get();
		 $result = $query->result();
		 if($result)
		 {
			
			 return $result[0]->SequenceName;
		 }
		 		 
	 }
	 
	 function  get_claim($id)
	 {
		 $ci=& get_instance();
         $ci->load->database();
		 $ci->db->select('claim_id,claim_name');
		 $ci->db->from('tbl_claim');
		 $ci->db->where('StepID',$id);
		 $query = $ci->db->get();
		 $result = $query->result();
		 if($result)
		 {
			
			 return $result;
		 }
		 		 
	 }
	 
	  function  get_claim_question($id)
	  {
		 $ci=& get_instance();
         $ci->load->database();
		 $ci->db->select('QuestionID,Question,ControlTypeID');
		 $ci->db->from('tblquestion_step_casesequence');
		 $ci->db->where('claim_id',$id);
		 $ci->db->where('IsActive',1);
		 $query = $ci->db->get();
		 $result = $query->result();
		 if($result)
		 {
			
			 return $result;
		 }
		 		 
	 }
	 
	 
	    function get_manager_name($id)
	    {
			$ci=& get_instance();
            $ci->load->database();			   
			$ci->db->select('FirstName');
			$ci->db->from('register_user');
			$ci->db->where('UserID',$id);
			$query = $ci->db->get();
		    $result = $query->result();
		     if($result)
		    {
			 foreach($result as $row)
			 {
				 $manager_name = $row->FirstName;
			 }
			 return $manager_name;
		    }
		 		 
	    }
		
	   function  get_user_law_firm($id)
	   {
		 $ci=& get_instance();
         $ci->load->database();
		 $ci->db->select('*');
		 $ci->db->from('tblis_law');
		 $ci->db->where('UserID',$id);
		 $ci->db->where('IsActive',1);
		 $query = $ci->db->get();
		 $result = $query->result();
		 if($result)
		 {
			
			 return $result;
		 }
		 		 
	 }
	 
	 function  get_user_company($id)
	   {
		 $ci=& get_instance();
         $ci->load->database();
		 $ci->db->select('*');
		 $ci->db->from('tblis_company');
		 $ci->db->where('UserID',$id);
		 $ci->db->where('IsActive',1);
		 $query = $ci->db->get();
		 $result = $query->result();
		 if($result)
		 {
			
			 return $result;
		 }
		 		 
	 }
		
		
		
	

   
?>