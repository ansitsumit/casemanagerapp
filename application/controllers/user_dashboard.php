<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_dashboard extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		 
		if ( ! $this->session->userdata('logged_in'))
        { 
            redirect('login');
        }
		$this->load->library('imagic_resize');
		$this->load->model('Dashboard_model');
		$this->load->model('case_model');
		$this->load->model('Notification_model');
		$this->load->model('user_model');
		

     }
	
	public function checkfirsttime($UserID,$User_type){
		if(!$this->session->userdata('ftime')){
			$this->session->set_userdata('ftime','TRUE');
			$countactivecase = $this->case_model->get_Active_case_list($UserID,$User_type);
			//print_r($countactivecase);
			//exit;
			
			if(count($countactivecase) == 1){
				redirect('user_dashboard/active_case/'.$countactivecase[0]->CaseID);	
			}
			
		}
	}
	
	public function index()
	{
		
		$data = array();	
		$session_data = $this->session->userdata('logged_in');
		$UserID = $session_data['UserID'];
		$User_type = $session_data['User_type'];
		$this->checkfirsttime($UserID,$User_type);
		$data['notifications'] = $this->Notification_model->show_notification(3);
		$data['active_case_list']=$this->case_model->get_Active_case_list($UserID);
		$data['close_case_list']=$this->case_model->get_close_case_list($UserID,$User_type);	
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$this->load->view('user_dashboard',$data);
	}
	
	    /* public function delete_vec()
		  {
			$vid = $this->input->get('vecId');
			$uid = $this->input->get('uid');
			$this->Dashboard_model->delete_vechicle($vid,$uid);
			redirect('dashboard');
	     }*/
	public function active_case($id=''){ 
		$data = array();
		 $session_data =  $this->session->userdata('logged_in');
		 $this->case_model->allowaccess($id,$session_data['UserID']);
		//$data['casestepstat'] = $this->case_model->getcasestat($id,$session_data['UserID']);
		//$partyno = $data['casestepstat'][0]->partyno;
		$partyno = $this->case_model->getpartybyuserid($id,$session_data['UserID']);
		$data['casemodule'] = $this->case_model->getinputmodules($id,$partyno);
		$data['casesequenceorder'] = $this->case_model->getcasesequenceorder($id);
		//echo $partyno."<br>";
		//print_r($data['casesequenceorder'] );
		//exit;
		//$data['userid']
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$data['caseItem']=$this->case_model->get_case($id);
		$this->load->view('close_case',$data);
		
	}	 

	public function close_case($id=''){ 
		$data = array();
		 $session_data =  $this->session->userdata('logged_in');
		$data['casestepstat'] = $this->case_model->getcasestat($id,$session_data['UserID']);
		
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$data['caseItem']=$this->case_model->get_case($id);
		$this->load->view('close_case',$data);
		
	}
	
	
	public function responseuser($caseid="",$moduleid=""){
		$sessdata = $this->session->userdata('logged_in');
		$userid = $sessdata['UserID'];
		$data['userid'] = $userid;
		$data['getpartyno'] = $this->case_model->hiddenpartyno($caseid,$userid);
		if (isset($_REQUEST['submitresponse'])){
			 $data['msg'] = $this->case_model->addresponse($userid,$caseid);
			 //print_r($data['msg']);
			 //exit;
			if($data['msg'] !== 0){
				$obj = 2;
				$objid = $userid;
				$role = 3;
				$action = 1;
				$comment = 'Answer submitted for CaseID '.$caseid;
				$this->Notification_model->addnotification($userid,$obj,$objid,$role,$action,$comment);
				$this->case_model->sendstepmail($caseid,$userid,$moduleid);
				$nextstep = $this->case_model->preprocessnextstep($caseid,$userid,$moduleid);
				//echo "<br>";
				//print_r($nextstep);
				//exit;
				if($nextstep == '1'){
					$this->session->set_flashdata('msg', 'Your next step is active now. Please continue');
					redirect('user_dashboard/active_case/'.$caseid);
				}elseif($nextstep == '2'){
					$this->session->set_flashdata('msg', 'Your next step is in pause state. Please wait till manager review your answer');
					redirect('user_dashboard/active_case/'.$caseid);
				}elseif($nextstep == 'lstep'){
					$this->session->set_flashdata('msg', 'Thank you for submitting');
					redirect('user_dashboard/active_case/'.$caseid);
				}
				//$this->session->set_flashdata('msg', 'Answers submitted successfully');
				//redirect('user_dashboard/active_case/'.$caseid);
			}else{
				$this->session->set_flashdata('msg', 'Some error occured');
			}
		}else{
			$data['caseid'] = $caseid;
			//$data['filledans'] = $this->case_model->getansbycaseid($caseid);
			
			//$data['getques'] = $this->case_model->getquesbycaseid($caseid);
			$data['getques'] = $this->case_model->getquesbymoduleid($moduleid);
			//$data['filledans'] = $this->case_model->getfileans($caseid,$userid);
			$data['filledans'] = $this->case_model->getansbyidquest($caseid,$userid,$data['getques']);
			$data['right_panel'] = $this->load->view('common/right_panel', '', true);
			$data['common_header'] = $this->load->view('common/header', '', true);
			$data['common_footer'] = $this->load->view('common/footer', '', true);
			$this->load->view('responseuser',$data);
		}
	}

	public function getdraftanswer($caseid,$quesid){
			$session_data =  $this->session->userdata('logged_in');
			$userid = $session_data['UserID'];
			$draft = $this->case_model->draftanswer($userid,$caseid,$quesid);
			echo $draft;
		
	}
	public function casepartydetail($id){
			$session_data =  $this->session->userdata('logged_in');
			$userid = $session_data['UserID'];
			$data['right_panel'] = $this->load->view('common/right_panel', '', true);
			$data['common_header'] = $this->load->view('common/header', '', true);
			$data['common_footer'] = $this->load->view('common/footer', '', true);
			$data['caseItem']=$this->case_model->get_case($id,$userid);
			$this->load->view('partydetails',$data);
	}

	public function getrepidbycaseseqid_userid($case_sequence_ID,$caseid,$userid=''){
		$utype = $this->input->post('utype',TRUE);
		if($userid == ''){
			$session_data = $this->session->userdata('logged_in');
			$userid = $session_data['UserID'];
		}
		
			$this->case_model->showrepid($case_sequence_ID,$caseid,$userid,$utype);
		//echo '1';
		
	}
	
	public function viewreport($RepID){
		
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$data['reportdata'] = $this->case_model->getreportdata($RepID);
		$data['modulename'] = $this->case_model->getcase_sequencenamebyid($data['reportdata'][0]->case_sequence_ID);
		$this->load->view('viewreport',$data);
	}
	
	public function showcaseseqorder($mcaseseqid,$caseid){
		$this->case_model->showcasesequenceorder($mcaseseqid,$caseid);
	}
	
	
	public function chgcaseseqbycaseid(){
		$session_data = $this->session->userdata('logged_in');
		$UserID = $session_data['UserID'];
		$this->user_model->adminmana();
		//print_r($this->input->post());
		$status = $this->case_model->chgcasesequenceorder($UserID);
		echo $status;
	}
	
	public function viewcasedetail($caseid){
		$session_data = $this->session->userdata('logged_in');
		$userid = $session_data['UserID'];
		$usertype = $session_data['User_type'];
		if($usertype == 3){
			$this->case_model->allowaccess($caseid,$userid);
		}
		$data['manager_list']=$this->case_model->get_manager_list();
		//$data['caseItem']=$this->case_model->get_case($id);
		$data['caseItem']=$this->case_model->get_case($caseid);
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$this->load->view('view_case',$data);		
	}
	
	public function saveresponse($caseid){
		$session_data = $this->session->userdata('logged_in');
		$userid = $session_data['UserID'];
		$this->case_model->allowaccess($caseid,$userid);
		//print_r($_FILES);
		$addresponse = $this->case_model->saveresponse($userid,$caseid);
		echo $addresponse;
	}
	
	public function sendchatmail($id){
		$this->case_model->sendchatmail($id);
	}

}



/* End of file welcome.php */
/* Location: ./application/controllers/user_dashboard.php */