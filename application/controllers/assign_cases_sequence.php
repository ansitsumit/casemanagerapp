<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assign_cases_sequence extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		 
		if ( ! $this->session->userdata('logged_in'))
        { 
            redirect('login');
        }
		$this->load->model('assign_model');
		$this->load->model('case_model');
		$this->load->model('user_model');
		//$this->user_model->roleaccess();
		$this->user_model->adminmana();
		//error_reporting(0);
     }

	public function index()
	{
		
		
		$data = array();
		//$data['case'] = $this->assign_model->getsequence();
		$data['case'] = $this->assign_model->getallsequence();
		//$data['case_sequence'] = $this->assign_model->get_case_sequence();
		///$data['input_module'] = $this->assign_model->get_input_module();
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$data['modcaseseqactive'] = $this->load->view('common/modcaseseqactive','',true);
		$data['modcaseseqlist'] = $this->load->view('common/modcaseseqlist', '', true);
		$this->load->view('sequence',$data);
		
	}
	

	public function chkcaseseqorder(){
		$mcaseseqid = $this->input->post('mcaseseqid',TRUE);
		$caseid = $this->input->post('caseid',TRUE);
		echo $this->assign_model->chkcaseseqorder($mcaseseqid,$caseid);
	}
	
	public function setaseseqorder(){
		$mcaseseqid = $this->input->post('mcaseseqid',TRUE);
		$caseid = $this->input->post('caseid',TRUE);
		 $this->case_model->setaseseqorder($mcaseseqid,$caseid);
		 //$this->case_model->setcasesequenstatus($mcaseseqid,$caseid);
		 //$this->case_model->getinputmodules($caseid);
		
	}
	
	
	public function updcaseseqsta(){
		//print_r($this->input->post());
		$caseseqid = $this->input->post('id');
		$val = $this->input->post('val');
		$type = $this->input->post('type');
		$caseid = $this->input->post('caseid');
		$userid = $this->input->post('userid');
			//if($type=='pause'){
			//	$dd = explode('chkpause',$id);
			//	$caseseqid = $dd[1];
			//}elseif($type=='continue_next'){
			//	$dd = explode('chkcont',$id);
			//	$caseseqid = $dd[1];
			//}elseif($type=='status'){
			//	$dd = explode('chkstat',$id);
			//	$caseseqid = $dd[1];
			//}
			//
			$this->case_model->updatecaseseqstat($caseseqid,$caseid,$userid,$type,$val);
				
			
	}
	
	public function Sequence_add()
	{
		if(isset($_POST['assign'])){
			$this->assign_model->add_assign();
		}
		
		$data = array();
		$data['case'] = $this->assign_model->get_case();
		$data['case_sequence'] = $this->assign_model->get_case_sequence();
		//$data['input_module'] = $this->assign_model->get_input_module();
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$this->load->view('assign_cases_sequence',$data);
	}
	
	public function deletesequence()
	{
		$uid = $this->input->post('Assign_ID');
		$res = $this->assign_model->delete($uid);
		echo $res;
		
	}
	
	
	
	public function Edit_sequence($id='',$caseid)
	{
		
		//print_r($data);
		//exit;
		if(isset($_POST['submit'])){
			$caseid = $this->input->post('CaseID');
			$mcaseseqid = $this->input->post('Case_Sequence_ID');
			$query = $this->db->get_where('assign_case_sequence',array('Case_ID'=>$caseid,'Main_case_sequence_ID'=>$mcaseseqid));
			if($query->num_rows() > 0){
				$this->session->set_flashdata('msg', 'Main case sequence is already assigned with same case');
				redirect('assign_cases_sequence');
			}else{
			$data['Main_case_sequence_ID'] = $this->input->post('Case_Sequence_ID');
			$data['Case_ID'] = $this->input->post('CaseID');
			$this->assign_model->update_assign($caseid,$data);
			}
		}
		else {
		//$data=array();
		$data['mcaseseqid'] = $id;
		$data['caseid'] = $caseid;
		$data['list'] = $this->assign_model->Edit_sequence($id);
		//print_r($data['list']);
		//exit;
		$data['case'] = $this->assign_model->get_case();
		$data['case_sequence'] = $this->assign_model->get_case_sequence();
		$data['right_panel'] = $this->load->view('common/right_panel', '', true);
		$data['common_header'] = $this->load->view('common/header', '', true);
		$data['common_footer'] = $this->load->view('common/footer', '', true);
		$this->load->view('edit_sequence',$data);
		//$user_data = $this->assign_model->Edit_sequence($id);
		//$msg=" successfully deleted";
		//$this->session->set_flashdata('msg',$msg);
		//redirect('assign_cases_sequence');
		
		}
	}
	
}

