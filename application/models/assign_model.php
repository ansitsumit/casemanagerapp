<?php

	class Assign_model extends CI_Model {

		function __construct()
		{
			parent::__construct();

		}
		// get case
		public function get_case(){
			$this->db->select('*');
			$this->db->where('CaseStatus != ',3);
			$query=$this->db->get('tblcase');
			//echo $this->db->last_query();
			
			return $query->result();
		}
		// get case sequence 
		public function get_case_sequence(){
			/* $query = "SELECT Main_case_sequence_ID, Case_Sequence_ID,SequenceName FROM case_sequence INNER JOIN  tblcasesequence ON case_sequence.Main_case_sequence_ID = tblcasesequence.CaseSequenceID";
			$query = $this->db->query($query);
			return $query->result(); */
			$this->db->select('*');
			$query=$this->db->get('tblcasesequence');
			return $query->result();
			
		}
		
		function getmcaseseqname($id){
			$this->db->select('SequenceName');
			$query = $this->db->get_where('tblcasesequence',array('CaseSequenceID'=>$id));
			$row = $query->row();
			if($query->num_rows()>0){
				return $row->SequenceName;
			}
			
		}
		
		public function getallsequence(){
			$query = $this->db->query('SELECT assign_case_sequence.Assign_ID,assign_case_sequence.Case_ID, assign_case_sequence.Main_case_sequence_ID, tblcase.CaseType  FROM `assign_case_sequence` JOIN tblcase ON assign_case_sequence.Case_ID = tblcase.CaseID');
			//$query = $this->db->get('assign_case_sequence');
			$jarray = array();
			foreach($query->result() as $row){
				$node= array();
				$node['Assign_ID'] = $row->Assign_ID;
				$node['Case_ID'] = $row->Case_ID;
				$node['CaseType'] = $row->CaseType;
				$node['SequenceName'] = $this->getmcaseseqname($row->Main_case_sequence_ID);
				$node['Main_case_sequence_ID'] = $row->Main_case_sequence_ID;
				array_push($jarray,$node);
			}
			
			return $jarray;
			
		}
		
		public function getsequence()
		{
		
			$query = "select assign_case_sequence.Assign_ID, assign_case_sequence.Main_case_sequence_ID, tblcase.CaseType ,tblcase.CaseID,tblcasesequence.SequenceName from tblcase 
INNER JOIN assign_case_sequence ON tblcase.CaseID=assign_case_sequence.Case_ID 
INNER JOIN tblcasesequence ON tblcasesequence.CaseSequenceID=assign_case_sequence.Main_case_sequence_ID";
			$query = $this->db->query($query);
			//echo $this->db->last_query();
			//exit;
			
			return $query->result();
		}
		//insert assign case sequence
		public function add_assign (){
			//print_r($this->input->post());
			//exit;
			$caseid = $this->input->post('CaseID');
			$mcaseseqid = $this->input->post('Case_Sequence_ID');
			$query = $this->db->get_where('assign_case_sequence',array('Case_ID'=>$caseid));
			if($query->num_rows() > 0){
				$this->session->set_flashdata('msg', 'Duplicate Data Case');
				redirect('assign_cases_sequence');
			}else{
			$data['Case_ID'] = $this->input->post('CaseID');
			$data['Main_case_sequence_ID'] = $this->input->post('Case_Sequence_ID');
			$this->db->insert('assign_case_sequence',$data);
			$this->session->set_flashdata('msg', 'Add successfuly');
			redirect('assign_cases_sequence');
			}
			
		}		
	function delete($id)
			{
				 $this->db->delete('assign_case_sequence', array('Assign_ID' => $id));
				if($this->db->affected_rows()>0)
				{
					return 1;
				}else{
					return 0;
				}
				 
			}
	
	
	function Edit_sequence($id)
			{
			$query = "select assign_case_sequence.Assign_ID,tblcase.CaseType ,tblcase.CaseID,tblcasesequence.SequenceName,tblcasesequence.CaseSequenceID from tblcase INNER JOIN assign_case_sequence ON tblcase.CaseID=assign_case_sequence.Case_ID INNER JOIN tblcasesequence ON tblcasesequence.CaseSequenceID=assign_case_sequence.Main_case_sequence_ID Where assign_case_sequence.Main_case_sequence_ID=$id";
			$query = $this->db->query($query);
			//return $this->db->last_query();
			
			return $query->result();
	
	
	
	
		}
		public function update_assign ($id,$data){
			//print_r($data);
			//exit;
			$query = $this->db->get_where('assign_case_sequence',$data);
			if($query->num_rows()>0){
				$this->session->set_flashdata('msg', 'Case is already assigned with same sequence');
			}else{
				$this -> db -> where('Case_ID',$id);
				$this -> db -> update('assign_case_sequence',$data);	
				$this->session->set_flashdata('msg', 'Update successfuly');
			}
			redirect('assign_cases_sequence');
			}
			
			
		public function chkcaseseqorder($mcaseseqid,$caseid){
			$query = $this->db->get_where('tblcasesequenceorder',array('mcaseseqid'=>$mcaseseqid,'caseid'=>$caseid));
			if($query->num_rows()>0){
				return 1;
			}else{
				return 0;
			}
		
		}
		
	}
?>