<?php

   class Case_model extends CI_Model {
	var $Question_ids=array();
	var $point=array();
	var $t_a=array();
	var $test_id=0;
    function __construct()
    {
        parent::__construct();
		 $this->load->library('email');
		 $this->load->helper('email');
		//error_reporting(0);
    }
			function login($userEmail, $userPassword)
			{
				
		
				$this -> db -> select('UserID,FirstName,LastName,Email,User_type ');
				$this -> db -> from('register_user');
				$this -> db -> where('Email',trim($userEmail));
				$this -> db -> where('Password',trim($userPassword));
				$this -> db -> where('IsActive = 1');
				$this -> db -> limit(1);
				$query = $this -> db -> get();
				//echo $this->db->last_query();
				if($query -> num_rows() == 1)
				{
				return $query->result();
				}
				else
				{
				return false;
				}
			}
			
			function checkExistance($userEmail)
			{
		
				$this -> db -> select('user_id');
				$this -> db -> from('users');
				$this -> db -> where('user_email = ' . "'" . $userEmail . "'");
				$this -> db -> where('status = 1');
				$query = $this -> db -> get();
				if($query -> num_rows() == 1)
				{
				return $query->row()->user_id;
				}
				else
				{
				return false;
				}
			}
			
	       function insertActivationcode($userEmail,$activation_id)
		    {
				 $data = array('pswd_reset_code' => $activation_id);
				 $this -> db -> where('user_email = ' . "'" . $userEmail . "'");
				 $res= $this->db->update('users', $data); 
				 if($res)
				 {
					 return 1;
				 }
				  else 
				  {
					  return 0;
				  }
		   }
		   
		    public function resetpass($pass,$resetcode)
			{
				$data = array('user_password ' => $pass,'pswd_reset_code ' => '');
				$this -> db -> where('pswd_reset_code = ' . "'" . $resetcode . "'");
				$res= $this->db->update('users', $data); 
			    if($res)
				 {
					 return 1;
				 }
				  else 
				  {
					  return 0;
				  }
			}
			
			
			public function get_case_sequence_list()
			{
			    $this -> db -> select('CaseSequenceID,SequenceName');
				$this -> db -> from('tblcasesequence');
				$query = $this -> db -> get();
				return $query->result();
			}
			
			public function get_edit_module($id){
				$this->db->select('*');
				$this->db->from('tblcasesequence');
				$this ->db->where('CaseSequenceID',$id );
				$query = $this->db->get();
				return $query->result();
			}
			
			public function update_module($id){
				$data = array();
				$data['SequenceName'] = $this->input->post('case_sequence_name');
				$this->db->where('CaseSequenceID',$id );
				$this->db->update('tblcasesequence',$data); 
			}
			
			public function get_input_module()
			{
			    $this -> db -> select('*');
				$this -> db -> from('case_sequence');
				$query = $this -> db -> get();
				return $query->result();
			}
			
			//public function get_case($id='')
			//{
			//    $this -> db -> select('*');
			//	$this -> db -> from('tblcase');
			//	 $this -> db -> where('CaseID',$id );
			//	$query = $this -> db -> get();
			//	return $query->result();
			//}

		public function get_case($caseid='',$userid='')
			{
			   if($userid == ''){
			    $this -> db -> select('*');
				$this -> db -> from('tblcase');
				 $this -> db -> where('CaseID',$caseid );
				$query = $this -> db -> get();
				return $query->result();
			   }else{
				  $this->db->select('fpid,spid');
				  $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
				  $row = $query->row();
				  if($query->num_rows()> 0){
					 if($userid == $row->fpid){
						$partycolumns = 'CaseTitle,CaseType,AmountInDispute,DateCommenced, FirstParty as partyname, AddressOfFirstParty as address, FirstPArtyPhone as phone,FirstPartyEmail as email ,AttorneyName as attname,AttorneyEmail as attemail, first_party_firm_name as firmname,AttorneyAddress as attaddress,AttorneyPhone as attphone';
					 }else if($userid == $row->spid) {
						 $partycolumns = 'CaseTitle,CaseType,AmountInDispute,DateCommenced,SecondParty as partyname, AddressOfSecondParty as address,SocendPArtyPhone as phone, SocendPartyEmail as email, second_party_attorney as attname, second_party_attorney_email as attemail,second_party_firm_name as firmname,second_party_attorney_address as attaddress,second_party_attorney_phone as attphone';
					 }
					 $this->db->select($partycolumns);
					 $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
					 return $query->result();
				  }else{
					 return 0;
				  }
			   }
			}			


			
			public function insert_case_sequence($data)
			{
			
			    $this->db->set($data);
                $result = $this->db->insert('tblcasesequence');
				return $insert_id = $this->db->insert_id();
				
			}
			
			public function insert_claim($data)
			{
			
			    $this->db->set($data);
                $result = $this->db->insert('tbl_claim');
				return $insert_id = $this->db->insert_id();
				
			}
			
			public function update_claim($id,$data)
			{
			    $this -> db -> where('claim_id',$id );
				$res = $this->db->update('tbl_claim',$data); 
			  
			}
			
			public function update_qustion($id,$data)
			{
			    $this -> db -> where('QuestionID',$id );
				$res = $this->db->update('tblquestion_step_casesequence',$data); 
			  
			}
			
			
			
			
			
			// to delete  case module 
	function delete_case_module($id)
			{
				 $rss = $this->db->delete('case_sequence', array('Case_Sequence_ID' => $id));
				
				 if($rss)
				 {
					 return 1;
				 }
				 else
				 {
					 return 0;
				 }
				 
			}
			public function insert_qustion($data)
			{
			
			    $this->db->set($data);
                $result = $this->db->insert('tblquestion_step_casesequence');
				return $insert_id = $this->db->insert_id();
				
			}
			
			
			
			public function insert_case_module($data)
			{
			
			    $this->db->set($data);
                $result = $this->db->insert('tblstep_casesequence');
				return $insert_id = $this->db->insert_id();
				
			}
			
			public function update_case_module($id,$data)
			{
			    $this -> db -> where('StepID',$id );
				$res = $this->db->update('tblstep_casesequence',$data);
				//echo $this->db->last_query();	
			}
			
			
			
			
			public function get_module_list()
			{
			    $this -> db -> select('*');
				$this -> db -> from('case_sequence');
				$query = $this -> db -> get();
				return $query->result();
		   	 
			}
			
			public function get_module_data($id)
			{
			    $this -> db -> select('*');
				$this -> db -> from('tblstep_casesequence');
				$this -> db -> where('StepID',$id);
				$query = $this -> db -> get();
				return $query->result();
		   	  
				
			}
			
			
			
			public function update_user($data,$id)
			{
				$this -> db -> where('UserID',$id );
				$res = $this->db->update('register_user',$data); 
			    if($res)
				{
				  return 'update';
				}
				else 
				{
				  return '';
				}
				
				
			}
			
			
			
			function get_user_list()
			{
				  $this->db->select('*');
				  $this->db->from('register_user');
				  $this->db->where('User_type',3);
				  $this->db->where('IsActive',1);
				  $this->db->order_by('UserJoinedDate','DESC');
				  $query=$this->db->get();
				  return $query->result();
			}
			
			function get_user($id)
			{
				  $this->db->select('*');
				  $this->db->from('register_user');
				  $this->db->where('UserID',$id);
				  $query=$this->db->get();
				  return $query->result();
			}
			
			
			function delete_question($id)
			{
				 $rss = $this->db->delete('tblquestion_step_casesequence', array('QuestionID' => $id));	
				 if($rss)
				 {
					 return 1;
				 }
				 else
				 {
					 return 0;
				 }
				 
			}
			
			function get_manager_list()
			{
				  $this->db->select('UserID,FirstName');
				  $this->db->from('register_user');
				  $this->db->where('User_type',2);
				  $this->db->where('IsActive',1);
				  $this->db->order_by('UserJoinedDate','DESC');
				  $query=$this->db->get();
				  return $query->result();
			}
			
			public function insert_cases($data)
			{
			    $this->db->set($data);
                $result = $this->db->insert('tblcase');
				//echo $this->db->last_query();
				return $insert_id = $this->db->insert_id();
				
			}
			
			public function update_case($id,$data)
			{
			    $this -> db -> where('CaseID',$id );
				$res = $this->db->update('tblcase',$data); 
				//echo $this->db->last_query();
			}
			public function delete_case($id)
			{
			   $rss = $this->db->delete('tblcase', array('CaseID' => $id));
			   if($rss)
			   {
				 return 1;   
			   }
			   else
			   {
				 return 0;
			   }
			}
			
			
			public function get_Active_case_list($user_id,$User_type='')
			{
			   $query = $this->db->query("SELECT * FROM tblcase WHERE IsActive = 1 AND (fpid = '$user_id' OR spid = '$user_id') AND CaseStatus = 2");
			      //$this->db->select('*');
				  //$this->db->from('tblcase');
				  //$this->db->where('IsActive',1);
				  //$this->db->where('fpid',$user_id);
				  //$this->db->or_where('spid',$user_id);				  
				  //$this->db->where('User_type',$user_type);
				  //$this->db->where('CaseStatus',2);
				  //$query=$this->db->get();
				  //return $this->db->last_query();
				  return $query->result();
			}
			public function get_admin_Active_case_list()
			{
				$this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->join('assign_case_sequence', 'tblcase.CaseID = assign_case_sequence.Case_ID','left');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('tblcase.CaseStatus',2);
				$query = $this->db->get();
				return $query->result();



				 /* $this->db->select('*');
				  $this->db->from('tblcase');
				  $this->db->where('IsActive',1);
				  $this->db->where('CaseStatus',2);
				  $query=$this->db->get();
				  return $query->result(); */
			}
			
			
			
			public function get_new_case_list($user_id,$user_type)
			{
			   $query = $this->db->query("SELECT * FROM tblcase WHERE IsActive = 1 AND (fpid = '$user_id' OR spid = '$user_id') AND CaseStatus = 1");
			      //$this->db->select('*');
				  //$this->db->from('tblcase');
				  //$this->db->where('IsActive',1);
				  //$this->db->where('fpid',$user_id);
				  //$this->db->or_where('spid',$user_id);
				  //$this->db->where('User_type',$user_type);
				  //$this->db->where('CaseStatus',1);
				  //$query=$this->db->get();
				  return $query->result();
			}
			
			public function get_admin_new_case_list()
			{
			    $this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('CaseStatus',1);
				$query = $this->db->get();
				return $query->result(); 

				 /* $this->db->select('*');
				  $this->db->from('tblcase');
				  $this->db->where('IsActive',1);
				  $this->db->where('CaseStatus',1);
				  $query=$this->db->get();
				  return $query->result(); */
			}
			/*avnish */
			public function get_manager_Active_case_list($uid)
			{
			   /*  $this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('tblcase.CaseStatus',1);
				$this->db->where('tblcase.ManagerAssigned',$uid);
				$query = $this->db->get();
				return $query->result();  */
				$this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->join('assign_case_sequence', 'tblcase.CaseID = assign_case_sequence.Case_ID','left');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('tblcase.CaseStatus',2);
				$this->db->where('tblcase.ManagerAssigned',$uid);
				$query = $this->db->get();
				return $query->result();

			}
			public function get_manager_close_case_list($uid)
			{
			     /*  $this->db->select('*');
				  $this->db->from('tblcase');
				  $this->db->where('IsActive',1);
				  $this->db->where('ManagerAssigned',$uid);
				  $this->db->where('CaseStatus',3);
				  $query=$this->db->get();
				  return $query->result(); */
				  $this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('ManagerAssigned',$uid);
				$this->db->where('CaseStatus',3);
				$query = $this->db->get();
				return $query->result();
			}
			/*end*/
			public function get_close_case_list($user_id,$user_type)
			{
			   $query = $this->db->query("SELECT * FROM tblcase WHERE IsActive = 1 AND (fpid = '$user_id' OR spid = '$user_id') AND CaseStatus = 3");
			//      $this->db->select('*');
			//	  $this->db->from('tblcase');
			//	  $this->db->where('IsActive',1);
			//	  $this->db->where('CreatedBy',$user_id);
			//	  $this->db->where('CaseStatus',3);
			//	  $query=$this->db->get();
				  return $query->result();
			}
			
			public function get_admin_close_case_list()
			{
			     $this->db->select('*');
				$this->db->from('register_user');
				$this->db->join('tblcase', 'register_user.UserID = tblcase.CreatedBy','inner');
				$this->db->where('tblcase.IsActive', 1);
				$this->db->where('CaseStatus',3);
				$query = $this->db->get();
				return $query->result();

				 /* $this->db->select('*');
				  $this->db->from('tblcase');
				  $this->db->where('IsActive',1);
				  $this->db->where('CaseStatus',3);
				  $query=$this->db->get();
				  return $query->result(); */
			}

			//add single question
			public function addsingleques($id){
			   ////print_r($this->input->post());
			   $this->db->trans_begin();
			   $options = $this->input->post('opt');
			   $t_a = $this->input->post('t_a');
			   $data['Case_Sequence_ID']=$id;
			   $data['Question']=$this->input->post('question');
			   $this->db->insert("test_quest",$data);
			   $s['IDQuest']=$this->db->insert_id();
			   $s['t_a'] =  $t_a;
			   $s['Case_Sequence_ID']=$id;
			   	  if($s['t_a'] == "Checkbox" || $s['t_a'] == "Radio"){
					$s['option_list'] =$options[0]."@".$options[1]."@".$options[2]."@".$options[3];
					}else{
					 $s['option_list'] = "@@@";
					}
					$this->db->insert("test_alternatives",$s);
					
					if ($this->db->trans_status() === FALSE){
						 $this->db->trans_rollback();
						 echo 0;
					 }else{
						 $this->db->trans_commit();
						 echo $id;
					 }
			}
			

			
			// add more submit module ##########################################################################
		
			function followupstep($caseid,$caseseqid,$partyno){
			   print_r($this->input->post());
			   //echo 'caseid'.$caseid;
			   //echo 'caseseqid'.$caseseqid;
			   //echo 'partyno'.$partyno;
			   
			   //print_r($this->input->post());
			   $query = $this->db->get_where('case_sequence',array('Case_Sequence_ID'=>$caseseqid));
			   if($query->num_rows()>0){
				  $row = $query->row();
				    $mcaseseqid = $row->Main_case_sequence_ID;
				  $input_module = $row->input_module;
				  $side_authority = $row->side_authority;
			   }
			   //print_r($row);
			   //exit;
			   //echo 'side autority'.$input_module;
			   //exit;
			   $data['Main_case_sequence_ID'] = $mcaseseqid;
			   $data['input_module'] = $input_module.': Follow Up';
			   $data['side_authority'] = $side_authority;
			   $this->db->insert("case_sequence",$data);
			   $test_id=$this->db->insert_id();;
			   $this->add_question($test_id);
			   $query = $this->db->get_where('tblcasesequenceorder',array('caseid'=>$caseid,'mcaseseqid'=>$mcaseseqid));
			   if($query->num_rows()>0){
				  $row1 = $query->row();
				  $orderid = $row1->orderid;
				  $caseorder  = $row1->casesequece_order;
			   }
			   //   print_r($row1);
			   //exit;
			   $partyarr = explode('@',$side_authority);

			   
			   //print_r($userid);
			   //exit;
			   
			   //getuserbyparty($caseid,$partyno)
			   
			   $neworder =  str_replace($caseseqid, $caseseqid.'@'.$test_id, $caseorder);
			   $this->db->update('tblcasesequenceorder',array('casesequece_order'=>$neworder),array('orderid'=>$orderid));
			   $this->db->update('tblcasesteps', array('orderby'=>$neworder),array('orderby'=>$caseorder));
			   foreach($partyarr as $partyarr):
				 $userid =  $this->getuserbyparty($caseid,$partyarr);
				 $data = array('party'=>$partyno,'caseid'=>$caseid,'caseseqid'=>$test_id,'created_on'=>date('Y-m-d h:i:s'),'orderby'=>$neworder,'status'=>1);
				 $this->db->insert('tblcasesteps',$data);
			   endforeach;
			   $this->add_question($test_id,1);
			   return $test_id;
			   
			}
		
			function add_test($id){
			$side = $this->input->post('side_authority');
			$side = implode('@',$side);
			$data = array(
			'Main_case_sequence_ID' => $this->input->post('main_case_sequence'),
			'input_module' => $this->input->post('input_module'),
			'side_authority' =>$side);
			if($id>0){
			  
			$this->db->where("Case_Sequence_ID",$id);
			$this->db->update("case_sequence",$data);
			$this->test_id=$id;
			}else{
				
			$this->db->insert("case_sequence",$data);
			$this->test_id=$this->db->insert_id();;
			}
			$this->add_question($this->test_id);

			}


			function add_question($id,$type=''){

			$question=$this->get_array_value($this->input->post("question",TRUE));
			$note=$this->get_array_value($this->input->post("notes",TRUE));
			$IDQuest=$this->get_array_value($this->input->post("IDQuest",TRUE));
			for($r=0;$r<count($question);$r++)
			{
			$data['Case_Sequence_ID']=$id;
			$data['Question']=$question[$r];
			$data['note'] = $note[$r];
			if($IDQuest[$r]>0){
			$this->db->where("IDQuest",$IDQuest[$r]);
			$this->db->update("test_quest",$data);
			$this->Question_ids[]=$IDQuest[$r];
			}
			else{
			$this->db->insert("test_quest",$data);
			$this->Question_ids[]=$this->db->insert_id();
			}
			}
			if($type == ''){
			   $this->add_test_alternatives();
			}
			
			}
			
			
			function get_array_value($post_valeu){
			   $array_value=array();
			   foreach($post_valeu as $data){
				   $array_value[]= $data;
			   }
			   return  $array_value;
			}

			function add_test_alternatives() {
			   $i = 0;
				$t_a=$this->get_array_value($_POST["t_a"]);
				//$options=$this->get_array_value($_POST["options"]);
				$iDAlternativa=$this->get_array_value($_POST["iDAlternativa"]);
				$options  = $this->input->post('options');
				$t=1;
				for($r=0;$r<count($this->Question_ids);$r++){
					$s['IDQuest']=$this->Question_ids[$r];
					$s['Case_Sequence_ID'] =$this->test_id;
					$s['t_a'] =$t_a[$r];
					//$s['option_list'] =$options[$r][0]."@".$options[$r][1]."@".$options[$r][2]."@".$options[$r][3];
					$s['option_list'] =$options[$s['IDQuest']][0]."@".$options[$s['IDQuest']][1]."@".$options[$s['IDQuest']][2]."@".$options[$s['IDQuest']][3];

				if($iDAlternativa[$r]>0){
					
					$this->db->where("iDAlternativa",$iDAlternativa[$r]);
					$this->db->update("test_alternatives",$s);
				}else{
					$options=$this->get_array_value($this->input->post("opt"));
					$options=array_chunk($options, 4);
					if($s['t_a'] == "Checkbox" || $s['t_a'] == "Radio"){
					$s['option_list'] =$options[$i][0]."@".$options[$i][1]."@".$options[$i][2]."@".$options[$i][3];
					$i++;
					}else{
					 $s['option_list'] = "@@@";
					}
					$this->db->insert("test_alternatives",$s);
					//echo $this->db->last_query();
				}
				$t++;
				}
			}
			
			public function get_case_sequence_all($id){
				$this->db->select('*');
				$this->db->from('case_sequence');
				$this->db->where('Case_Sequence_ID', $id);
				$query = $this->db->get();
				return $query->result();
			
			}
			// question
			public function get_qustion($id){
				$role1="";
				$query = $this->db->query("select * from test_quest 
				inner join  test_alternatives on test_alternatives.IDQuest = test_quest.IDQuest  
				where   test_quest.Case_Sequence_ID='".$id."'");
				foreach($query->result() as $role)
				{
				$role1[]=$role;
				}

				return $role1;
			
			}
			
			public function get_input($q){
				$role1="";
				$q = intval($_GET['q']);
				$query = $this->db->query("select * from test_quest 
				inner join  test_alternatives on test_alternatives.IDQuest = test_quest.IDQuest  
				where test_quest.Case_Sequence_ID='".$q."'");
				
				foreach($query->result() as $role)
				{
				$role1[]=$role;
				}

				return $role1;
			
			}
			
			public function get_value($q){
				//$role1="";
				//$q = intval($_GET['q']);
				//$query = $this->db->query("select * from tblcasesequence 
				//inner join  case_sequence on tblcasesequence.CaseSequenceID = case_sequence.Main_case_sequence_ID  
				//where tblcasesequence.CaseSequenceID='".$q."'");
				$query = $this->db->select('Case_Sequence_ID,input_module')->from('case_sequence')->where('Main_case_sequence_ID',$q)->get();
			   $json_respose = array();
			   foreach($query->result() as $row):
			   $res['Case_Sequence_ID'] = $row->Case_Sequence_ID;
			   $res['input_module'] = $row->input_module;
			   array_push($json_respose,$res);
			   endforeach;
			   
			   return json_encode($json_respose);
			
			}
			
			// responce tab start here .............
			public function add_responce($id){
				
				$side = $this->input->post('side_authority');
				$side = implode('@',$side);
				$data = array(
				'Main_case_sequence_ID' => $this->input->post('main_case_sequence'),
				'input_module' => $this->input->post('input_module'),
				'side_authority' =>$side);
				if($id>0){
				  
				$this->db->where("responce_ID",$id);
				$this->db->update("responce_sequence",$data);
				$this->test_id=$id;
				}else{
				$this->db->insert("responce_sequence",$data);
				$this->test_id=$this->db->insert_id();;
				}
				$this->responce_question($this->test_id);
			}
			public function responce_question($id){
				$question=$this->get_array_value($_POST["question"]);
				$IDQuest=$this->get_array_value($_POST["IDQuest"]);
				for($r=0;$r<count($question);$r++)
				{
				$data->responce_ID=$id;
				$data->Question=$question[$r];

				if($IDQuest[$r]>0){
				$this->db->where("IDQuest",$IDQuest[$r]);
				$this->db->update("responce_quest",$data);
				$this->Question_ids[]=$IDQuest[$r];;
				}
				else{
				$this->db->insert("responce_quest",$data);
				$this->Question_ids[]=$this->db->insert_id();;
				}
				}
				$this->responce_alternatives();
				
			}
			
			public function responce_alternatives(){
				$t_a=$this->get_array_value($_POST["t_a"]);
				$options=$this->get_array_value($_POST["options"]);
				$iDAlternativa=$this->get_array_value($_POST["iDAlternativa"]);
				$t=1;
				$options=array_chunk($options, 4);
				for($r=0;$r<count($this->Question_ids);$r++){
					$s->IDQuest=$this->Question_ids[$r];
					$s->responce_ID=$this->test_id;
					$s->t_a =$t_a[$r];
					$s->option_list=$options[$r][0]."@".$options[$r][1]."@".$options[$r][2]."@".$options[$r][3];

				if($responceid[$r]>0){
					$this->db->where("responceid",$responceid[$r]);
					$this->db->update("responce_alternatives",$s);
				}else{
					
					$this->db->insert("responce_alternatives",$s);
				}
				$t++;
				}
				
			}
			
			//ans list case view for module
			
			public function get_ans_list($id){
				
				$query = "SELECT * FROM  assign_case_sequence 
				INNER JOIN  case_sequence ON assign_case_sequence.Main_case_sequence_ID = case_sequence.Main_case_sequence_ID 
				INNER JOIN  test_quest ON case_sequence.Case_Sequence_ID = test_quest.Case_Sequence_ID 
				INNER JOIN  test_alternatives ON test_quest.IDQuest = test_alternatives.IDQuest Where assign_case_sequence.Case_ID='".$id."' and case_sequence.side_authority like '1%'";
				$query = $this->db->query($query);
				return $query->result();
			}
			
			public function get_ans_list_sec($id){
				
				$query = "SELECT * FROM  assign_case_sequence 
				INNER JOIN  case_sequence ON assign_case_sequence.Main_case_sequence_ID = case_sequence.Main_case_sequence_ID 
				INNER JOIN  test_quest ON case_sequence.Case_Sequence_ID = test_quest.Case_Sequence_ID 
				INNER JOIN  test_alternatives ON test_quest.IDQuest = test_alternatives.IDQuest Where assign_case_sequence.Case_ID='".$id."' and case_sequence.side_authority like '%2%'";
				$query = $this->db->query($query);
				return $query->result();
			}
			
			//ans list case view for responce 
			public function get_responce_list($id , $id2){
				$query = "SELECT * FROM  responce_sequence
				INNER JOIN  responce_quest ON responce_sequence.responce_ID = responce_quest.responce_ID 
				INNER JOIN   responce_alternatives ON responce_quest.IDQuest = responce_alternatives.IDQuest
				Where Main_case_sequence_ID='".$id2."' and  responce_sequence.side_authority like '1%'";
				$query = $this->db->query($query);
				return $query->result();
			}
			
			public function get_responce_list_sec($id){
				
				$query = "SELECT * FROM  responce_sequence
				INNER JOIN  responce_quest ON responce_sequence.responce_ID = responce_quest.responce_ID 
				INNER JOIN   responce_alternatives ON responce_quest.IDQuest = responce_alternatives.IDQuest
				Where Main_case_sequence_ID='".$id."' and  responce_sequence.side_authority like '%2%'";
				$query = $this->db->query($query);
				return $query->result();
			}
			
			public function casestat($name,$userid,$type,$caseid,$status,$mid){
			   $this->load->model('notification_model');
			   
			   $obj = 3;
			   $objid= $caseid;
			   $role = $type;
			   if($type== 2 && $status == 1){
				  //$no =1;
				  $action = 2;
				  $comment = "Manager ".$name." has rejected case <strong>CaseID ".$caseid."</strong>";
			   
			   }else if($type == 1 && $status == 2){
				  //$no =2;
				  $action = 2;
				  $comment = "Admin ".$name." has assigned the case <strong>CaseID".$caseid."</strong> to ".$this->notification_model->getnamebyid($mid);
			   
			   }else if($type ==2 && $status == 2){
				 //$no =3;
				  $action = 2;
				  $comment = "Manager ".$name." has update the case <strong>CaseID".$caseid."</strong> to ".$this->notification_model->getnamebyid($mid);				  
			   }else if($type== 1 && $status == 1){
				  //$no =4;
				  $action = 2;
				  $comment = "Admin ".$name." has reverted the case <strong>CaseID ".$caseid."</strong>";
			   
			   }
			   
			   else if($type== 1 && $status == 3){
				  //$no =5;
				  $action = 2;
				  $comment = "Admin ".$name." has Change the case Status <strong>CaseID ".$caseid."</strong>";
			   
			   }
			    else if($type== 1 && $status == ''){
				  //$no =6;
				  $action = 2;
				  $comment = "Admin ".$name." has assigned the case <strong>CaseID ".$caseid."</strong>";
			   
			   }
			   else if($type== 2 && $status == '3'){
				  //$no =7;
				  $action = 2;
				  $comment = "Admin ".$name." has assigned the case <strong>CaseID ".$caseid."</strong>";
			   
			   }
			   
			  else if($type== 2 && $status == ''){
				  //$no =8;
				  $action = 2;
				  $comment = "Admin ".$name." has update the case <strong>CaseID ".$caseid."</strong>";
			   
			   }
				//echo $type;
				//echo $no;
				//exit;
			   $this->notification_model->addnotification($userid,$obj,$caseid,$type,$action,$comment);
			   
			}
			
			public function delcasenotification($name,$userid,$role,$case_id){
			   $comment= 'Admin '.$name.' deleted the case <strong>CaseID '.$case_id.'</strong>';
			   $this->notification_model->addnotification($userid,3,$case_id,$role,3,$comment);
			   
			}
			
			public function getmanagercomment($caseid,$partyno){
			   $this->db->select('*');
			   $this->db->from('tab_comment');
			   $this->db->where('caseid',$caseid);
			   $this->db->where('partyno',$partyno);
			   $query = $this->db->get();
			   return $query->result();
			   
			}
			
			
			public function getmancom($caseid,$moduleid){
			   $query = $this->db->get_where('tab_comment',array('caseid'=>$caseid,'stepno'=>$moduleid));
			   return $query->result();
			}
			
			public function viewmanagercomm($caseid,$moduleid){
			   //if($partyno !== '1@2'){
			   //
			   //}
			   $query = $this->db->get_where('tab_comment',array('caseid'=>$caseid,'stepno'=>$moduleid));
			   return $query->result();
			}
			
			public function delmanacom($id){
			   $this->db->delete('tab_comment',array('commentid'=>$id));
			   if($this->db->affected_rows() > 0 ){
				  return 1;
			   }else{return 0;}
			   
			}
			
			public function allcasestat(){
			   $query = $this->db->get_where('tab_status',array('active'=>1));
			   return $query->result();
			}
			
			public function chgcasestate($caseid,$stepno,$partyno,$status,$review){
			   $query = $this->db->get_where('tab_casestep',array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno));
			   if($query->num_rows() > 0){
				  $row = $query->row();
				  if($status == $row->status &&  $review == $row->is_reviewed){
					 $res = 2;
				  }else{
					 $data = array('status'=>$status,'is_reviewed'=>$review);
					 $this->db->update('tab_casestep',$data,array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno));
					 if($this->db->affected_rows() > 0){$res = 1;}else{$res = 0;}
				  }
			   }else{
				  $data = array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno,'status'=>$status,'is_reviewed'=>$review);
				  $this->db->insert('tab_casestep',$data);
				  if($this->db->affected_rows() > 0){$res =1;}else{$res =0;}
			   }
			   echo $res;
			   
			}
			
			public function stepdata($caseid,$partyno,$stepno){
			   //$query = $this->db->get_where('tab_casestep',array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno));
			   $query = $this->db->query('SELECT tab_casestep.is_reviewed, tab_casestep.status, tab_status.status_text FROM tab_casestep JOIN tab_status ON tab_casestep.status = tab_status.id AND tab_casestep.caseid = "'.$caseid.'" AND partyno ="'.$partyno.'" AND  tab_casestep.stepno ="'.$stepno.'" ');
			   if($query->num_rows() > 0 ){
			   return $query->row();
			   
			   }
			}
			
			public function getpartyemail($caseid,$partyno){
			   if($partyno == 1){
				  $partemail = 'FirstPartyEmail AS email';
			   }else{
				  $partemail = 'SocendPartyEmail AS email';
			   }
			   $this->db->select($partemail)->from('tblcase')->where('CaseID',$caseid);
			   $query = $this->db->get();
			   return $query->row();
			}
			
			public function getcasestat($caseid,$userid){
			   $sql = $this->db->get_where('tab_casestep',array('caseid'=>$caseid));
			   if($sql->num_rows() > 0){
			   
				  $this->db->select('fpid,spid');
				  $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
				  $row =$query->row();
				  if($row->fpid == $userid){
				  $partyno = 1;
				  }else if($row->spid == $userid){
					 $partyno = 2;
				  }
				  $this->db->order_by("stepno", "ASC");
				  $query = $this->db->get_where('tab_casestep',array('caseid'=>$caseid,'partyno'=>$partyno));
				  return $query->result();
			   }else{
				  return 0;
			   }
			}
			
			function getlastcaseseqid($caseid){
			   //$query = $this->db->query("SELECT case_sequence.Case_Sequence_ID FROM case_sequence JOIN assign_case_sequence ON case_sequence.Main_case_sequence_ID = assign_case_sequence.Main_case_sequence_ID AND assign_case_sequence.Case_ID ='$caseid' ORDER BY case_sequence.Case_Sequence_ID DESC LIMIT 1");
			   $query = $this->db->query("SELECT case_sequence.Case_Sequence_ID FROM case_sequence JOIN assign_case_sequence ON case_sequence.Main_case_sequence_ID = assign_case_sequence.Main_case_sequence_ID AND assign_case_sequence.Case_ID ='$caseid' ORDER BY case_sequence.Case_Sequence_ID DESC");
			//   if($query->num_rows() > 0 ){
			//   $row = $query->row();
			//   return $row->Case_Sequence_ID;
			//   }else{
			//	  return false;
			//   }
			return $query->result();
			
			}
			
			
			public function getquesbycaseid($caseid){
			   $caseseqid = $this->getlastcaseseqid($caseid);
			   $getqu = array();
			   foreach($caseseqid as $caseseqid){
			   $cqid = $caseseqid->Case_Sequence_ID;
			   $query = $this->db->query("select * from test_quest inner join test_alternatives on test_alternatives.IDQuest = test_quest.IDQuest where test_quest.Case_Sequence_ID= '$cqid'");
			   //echo $this->db->last_query();
			   $json_respose = array();
			   foreach($query->result() as $row):
			   $res['IDQuest'] = $row->IDQuest;
			   $res['Case_Sequence_ID'] = $row->Case_Sequence_ID;
			   $res['side_auth'] = $this->getside($row->Case_Sequence_ID);
			   $res['Question'] = $row->Question;
			   $res['iDAlternativa'] = $row->iDAlternativa;
			   $res['option_list'] = $row->option_list;
			   $res['t_a'] = $row->t_a;
			   array_push($json_respose,$res);
			   
			   //print_r($json_respose);
			   endforeach;
			   array_push($getqu,$json_respose);
			   }
			   
			   
			   //return $json_respose;
			   return $getqu;
			}
			
			function getside($id){
			   $query = $this->db->select('side_authority')->where('Case_Sequence_ID',$id)->get('case_sequence');
			   return $query->row();
			}
			
			public function get_manager_comment_for_admin($caseid,$partyno){
			   $this->db->select('*');
			   $this->db->from('tab_comment');
			   $this->db->where('caseid',$caseid);
			   $this->db->where('partyno',$partyno);
			   $query = $this->db->get();
			  //echo $this->db->last_query();
			   
			   return $query->result();
			   
			}
			
			
			public function getmychkbox($ind,$data){
			   $input = preg_quote($ind, '~'); // don't forget to quote input string!
			   //echo "data<br>";
			   //print_r($data);
			   $result = preg_grep('~' . $input . '~', $data);
			   return str_replace($input.'q','',implode($result,"@"));
			}
			
			
			public function getansbycaseid($caseid){
			   $query = $this->db->get_where('caseanswer',array('caseid'=>$caseid));
			   if($query->num_rows() > 0){
			   return $query->result();
			   }else{
				  return 0;
			   }
			}
			
			public function getfileans($caseid,$userid){
			   $query = $this->db->query("SELECT caseanswer.IDQuest,caseanswer.anstxt, file_upload.f_name from caseanswer LEFT JOIN file_upload ON caseanswer.anstxt = file_upload.refrenceid WHERE caseanswer.caseid = '$caseid' AND caseanswer.userid='$userid'  ORDER BY caseanswer.IDQuest ASC");
			   if($query->num_rows() > 0){
			   return $query->result();
			   }else{
				  return 0;
			   }
			}   

			function getansbyidquest($caseid,$userid,$arr){
			   foreach($arr as $arr):
			   $newarr[] = $arr->IDQuest;
			   endforeach;
			   //return $newarr;
			   $this->db->where('caseid',$caseid);
			   $this->db->where('userid',$userid);
			   $this->db->where_in('IDQuest',$newarr);
			   $query = $this->db->get('caseanswer');
			   //return $this->db->last_query();
			   //return $query->result();
			   $json_respose = array();
			   $node = array();
			   foreach($query->result() as $result):
			   
			   $node['ansid'] = $result->ansid;
			   $node['caseid'] = $result->caseid;
			   $node['userid'] = $result->userid;
			   $node['IDQuest'] = $result->IDQuest;
			   $node['t_a'] = $result->t_a;
			   $node['iDAlternativa'] = $result->iDAlternativa;
			   $node['optionlist'] = $result->optionlist;
			   $node['anstxt'] = $result->anstxt;
			   $node['saveanswer'] = $result->saveanswer;
			   $node['userquery'] = $result->userquery;
			   if($result->t_a == 'file'){
				  $node['fname'] = $this->getfnamebyref($result->anstxt);
			   }else{
				  $node['fname'] = '';
			   }
			   array_push($json_respose,$node);
			   endforeach;
			   return $json_respose;
			
			}

			function draftanswer($userid,$caseid,$quesid){
			   $query = $this->db->get_where('caseanswer',array('caseid'=>$caseid,'userid'=>$userid,'IDQuest'=>$quesid));
			   return json_encode($query->result());
			}

			function getfnamebyref($refid){
			   $query = $this->db->select('f_name')->from('file_upload')->where('refrenceid',$refid)->get();
			   if($query->num_rows() > 0){
				  $row = $query->row();
				  return $row->f_name;
			   }
			}
			

			function getsingleanswerbyidquestnuser($idquest,$userid){
			   $query = $this->db->select('anstxt')->get_where('caseanswer',array('IDQuest'=>$idquest,'userid'=>$userid));
			   $row =  $query->row();
			   if($query->num_rows()>0){
				return $row->anstxt;
			   }
			}
			
			function getfilesbyref($refid){
			   $query = $this->db->select('f_name,f_type')->from('file_upload')->where('refrenceid',$refid)->get();
			   return $query->result();
			}
			
			function delete_case_sequence($id){
                 $rss = $this->db->delete('tblcasesequence', array('CaseSequenceID' => $id));
                
                 if($rss)
                 {
                     return 1;
                 }
                 else
                 {
                     return 0;
                 }
                 
            }

      function get_manager_list_for_admin($id)
			{
			   //error_reporting(0);
				 
			   $this->db->select('FirstName');
				$this->db->from('register_user');
				$this->db->join('tblcase','tblcase.ManagerAssigned = register_user.UserID','inner');
				$this->db->where('tblcase.CaseID',$id);
				$query = $this->db->get();
				
				return $query->result();
   				 
			}
			
			public function stepdata_for_admin($caseid){
			   //$query = $this->db->get_where('tab_casestep',array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno));
			   $query = $this->db->query('SELECT tab_casestep.is_reviewed, tab_casestep.status, tab_status.status_text FROM tab_casestep JOIN tab_status ON tab_casestep.status = tab_status.id where tab_casestep.caseid = "'.$caseid.'" ');
			 
			//echo $this->db->last_query();
			 return $query->row();
			}

	  public function addresponse($userid,$caseid){
		 //$idqust = $this->input->post('IDQuest');
			   //print_r($idqust);
			   //print_r($_FILES);
			   //exit;
			   $alldata = $this->input->post();
			   $idqust = array();
			   $idalternative = array();
			   $optionlist = array();
			   $ta = array();
			   $anstxt = array();
			   $idqust = $this->input->post('IDQuest');
			   $idalternative = $this->input->post('iDAlternativa');
			   $optionlist = $this->input->post('option_list');
			   $ta = $this->input->post('ta');
			   $anstxt = $this->input->post('anstxt');
			   $ansrad = $this->input->post('ansrad');
			   //$ansquey = $this->input->post('') 			   "anstxt $getqu->IDQuest[] 
			   $i = 0;
			   $this->db->trans_begin();
			   foreach($idqust as $idqust){
				  if($ta[$i] == "Checkbox"){
					$atxt = $this->getchkbox($alldata['anschk'.$idqust]);
				  }else if($ta[$i] == "Radio"){
					$atxt = $alldata['ansrad'.$idqust];
				  }else if($ta[$i] == "file"){
					 $atxt = $this->uploadimg($userid,$caseid,$idqust,$_FILES['file'.$idqust]);
				  }else if($ta[$i] == "Text"){
					$atxt = $alldata['anstxt'.$idqust][0];
				  }
				  $ansquery = $alldata['ansquery'.$idqust][0];

				  if($ta[$i] == 'file' && $atxt == 0){
					 $data = array('userid'=>$userid,'caseid'=>$caseid,'IDQuest'=>$idqust,'t_a'=>$ta[$i],'iDAlternativa'=>$idalternative[$i],'optionlist'=>$optionlist[$i],'userquery'=>$ansquery);
				  }else{
					$data = array('userid'=>$userid,'caseid'=>$caseid,'IDQuest'=>$idqust,'t_a'=>$ta[$i],'iDAlternativa'=>$idalternative[$i],'optionlist'=>$optionlist[$i],'anstxt'=>$atxt,'userquery'=>$ansquery); 
				  }
				  

				  $op = $this->checkans($caseid,$idqust,$userid);
				  if($op == 0){
				  $this->db->insert('caseanswer',$data);
				  }else{
					 
					 $this->db->update('caseanswer',$data,array('caseid'=>$caseid,'userid'=>$userid,'IDQuest'=>$idqust));
				  }
				  $i++;
			   }
			   if ($this->db->trans_status() === FALSE){
					  $this->db->trans_rollback();
					  return 0;
				  }else{
					  $this->db->trans_commit();
					  
					  return 1;
				  }
			   
			}

	  public function saveresponse($userid,$caseid){
		 //$idqust = $this->input->post('IDQuest');
			   //print_r($idqust);
			   //print_r($_FILES);
			   //exit;
			   $alldata = $this->input->post();
			   $idqust = array();
			   $idalternative = array();
			   $optionlist = array();
			   $ta = array();
			   $anstxt = array();
			   $idqust = $this->input->post('IDQuest');
			   $idalternative = $this->input->post('iDAlternativa');
			   $optionlist = $this->input->post('option_list');
			   $ta = $this->input->post('ta');
			   $anstxt = $this->input->post('anstxt');
			   $ansrad = $this->input->post('ansrad');
			   $i = 0;
			   $this->db->trans_begin();
			   foreach($idqust as $idqust){
				  if($ta[$i] == "Checkbox"){
					$atxt = $this->getchkbox($alldata['anschk'.$idqust]);
				  }else if($ta[$i] == "Radio"){
					$atxt = $alldata['ansrad'.$idqust];
				  }else if($ta[$i] == "file"){
					 $atxt = $this->uploadimg($userid,$caseid,$idqust,$_FILES['file'.$idqust]);
				  }else if($ta[$i] == "Text"){
					$atxt = $alldata['anstxt'.$idqust][0];
				  }
				  $ansquery = $alldata['ansquery'.$idqust][0];

				  if($ta[$i] == 'file' && $atxt == 0){
					 $data = array('userid'=>$userid,'caseid'=>$caseid,'IDQuest'=>$idqust,'t_a'=>$ta[$i],'iDAlternativa'=>$idalternative[$i],'optionlist'=>$optionlist[$i],'userquery'=>$ansquery);
				  }else{
					$data = array('userid'=>$userid,'caseid'=>$caseid,'IDQuest'=>$idqust,'t_a'=>$ta[$i],'iDAlternativa'=>$idalternative[$i],'optionlist'=>$optionlist[$i],'saveanswer'=>$atxt,'userquery'=>$ansquery); 
				  }
				  

				  $op = $this->checkans($caseid,$idqust,$userid);
				  if($op == 0){
				  $this->db->insert('caseanswer',$data);
				  }else{
					 
					 $this->db->update('caseanswer',$data,array('caseid'=>$caseid,'userid'=>$userid,'IDQuest'=>$idqust));
				  }
				  $i++;
			   }
			   if ($this->db->trans_status() === FALSE){
					  $this->db->trans_rollback();
					  return 0;
				  }else{
					  $this->db->trans_commit();
					  
					  return 1;
				  }
			   
			}


			public function getchkbox($arr){
			   return implode($arr,"@");
			}
			
			
			function checkans($caseid,$idqust,$userid){
			   $query = $this->db->get_where('caseanswer',array('caseid'=>$caseid,'IDQuest'=>$idqust,'userid'=>$userid));
				  if($query->num_rows() > 0){
					 return 1;
				  }else{
					 return 0;
				  }
			}
			
			
		//UPDATE `caseanswer` SET `userid` = '2', `caseid` = '2', `IDQuest` = '26', `t_a` = 'Text', `iDAlternativa` = '25', `optionlist` = '@@@', `anstxt` = 'fgdffd dsdsd d', `userquery` = '' WHERE `caseid` = '2' AND `userid` = '2' AND `IDQuest` = '26'	
   

function uploadimg($userid,$caseid,$qid,$file){
   $files = array();
   $refrenceid =  $userid.$caseid.$qid.uniqid();
	 if (!empty($_FILES)) {
	  foreach ($file as $key=>$value){
		$keyval = array($key => $value);
		array_push($files,$keyval);
	  }
	for($i=0;$i<count($value);$i++){
	  //echo $file['name'][$r]."<br>";
	    $tempFile = $file['tmp_name'][$i];
		//checking extension
		$array = explode('.', $file['name'][$i]);
		$extension = end($array);
		$allow_ext = array('jpg','png','jpeg','gif','bmp','pdf','doc','docx');
		if(in_array($extension,$allow_ext)){
         // using DIRECTORY_SEPARATOR constant is a good practice, it makes your code portable.
		   $targetPath = getcwd(). '/uploads/';  
		 // Adding timestamp with image's name so that files with same name can be uploaded easily.
		$file_name=time().$userid.$caseid.$qid.$i.'.'.$extension;
		$fname =  $targetPath.$file_name;  
		$ftype=$file["type"][$i];
		$fsize=($file["size"][$i] / 1024);
	$arr= array('f_name'=>$file_name,
			'f_size'=>$fsize,
			'f_type'=>$ftype,
			'd_date'=>date('Y-m-d h:i:s'),
			'refrenceid'=>$refrenceid,
			);
		$this->db->insert('file_upload', $arr);
		move_uploaded_file($tempFile,$fname); 
		}
	  }
	 }
	 return $refrenceid;
 }  
  
  
  
  
   function casequesans($id){
	  
   //$query = $this->db->get_where('test_alternatives',array('Case_Sequence_ID'=>$id));
   $query = $this->db->query("SELECT test_quest.IDQuest, test_alternatives.IDQuest, test_alternatives.option_list,test_alternatives.t_a, test_quest.note, test_quest.Question FROM `test_alternatives` JOIN test_quest ON test_alternatives.IDQuest = test_quest.IDQuest WHERE test_quest.Case_Sequence_ID = '$id'");
   return $query->result();
   
  }

  function getuserbyparty($caseid,$partyno){
   if($partyno == "1"){
	  $cp = 'fpid';
   }else{
	  $cp = 'spid';
   }
   $query = $this->db->select($cp)->get_where('tblcase',array('CaseID'=>$caseid));
    //$query = $this->db->query("select ".$cp."  from tblcase where CaseID = '$caseid'");
	$row = $query->row();
	if($query->num_rows() > 0){
	return $row->$cp;
	}else{
	  return 0;
	}
  }
  
  function hiddenpartyno($caseid,$userid){
	  $query = $this->db->select('fpid,spid')->get_where('tblcase',array('CaseID'=>$caseid));
	  $row = $query->row();
	  if($query->num_rows() > 0 ){
		 if($userid ==  $row->fpid){
			return 2;
		 }elseif ($userid ==  $row->spid){
			return 1;
		 }
	  }
	  
  }
  
  function getpartybyuserid($caseid,$userid){
	  $query = $this->db->select('fpid,spid')->get_where('tblcase',array('CaseID'=>$caseid));
	  $row = $query->row();
	  if($query->num_rows() > 0 ){
		 if($userid ==  $row->fpid){
			return 1;
		 }elseif ($userid ==  $row->spid){
			return 2;
		 }
	  }
  }
  
  function getunregisteredemail(){
	  $query = $this->db->query("SELECT CaseID, CaseType,FirstParty,SecondParty,DateCommenced,CaseStatus,fpid,spid,FirstPartyEmail, SocendPartyEmail FROM tblcase WHERE fpid IS NULL || spid IS NULL || spid = '0' || fpid = '0'");
	  return $query->result();
  }
  
  public function getunregisterpartydata($caseid,$party){
   if($party == 1){
	  $this->db->select('CaseID, FirstParty as name, AddressOfFirstParty as address, FirstPartyEmail as email, FirstPArtyPhone as phone');
   }elseif($party == 2){
	  $this->db->select('CaseID, SecondParty as name, AddressOfSecondParty as address, SocendPartyEmail as email, SocendPArtyPhone as phone');
   }
   $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
   return $query->row();
  }
  
  public function updfpspid($email,$inserted_id){
	  $query = $this->db->query("SELECT CaseID, fpid,spid FROM tblcase WHERE (fpid IS NULL OR spid IS NULL OR fpid = '0' OR spid = '0') AND (FirstPartyEmail = '$email' OR SocendPartyEmail = '$email')");
	  foreach($query->result() as $row){
	  if($row->fpid == '' || $row->fpid == '0'){
		 $data = array('fpid'=>$inserted_id);
		 $this->db->update('tblcase',$data,array('CaseID'=>$row->CaseID));
	  }elseif($row->spid == '' || $row->spid == '0'){
		 $data = array('spid'=>$inserted_id);
		 $this->db->update('tblcase',$data,array('CaseID'=>$row->CaseID));
		 
	  }
	  
	  }
  }

  

	  function stepstatus($caseid,$stepno,$partyno){
		   $query = $this->db->get_where('tab_casestep',array('caseid'=>$caseid,'stepno'=>$stepno,'partyno'=>$partyno));
		   if($query->num_rows() > 0){
			$row = $query->row();
			return $row;
		   }else{
			return 0;
		   }
	  
	  }
  
  

   public function getcasesequenceorder($caseid){
	  $query = $this->db->get_where('tblcasesequenceorder',array('caseid'=>$caseid));
	  if($query->num_rows()>0){
		 $row = $query->row();
		 $caseseq = array();
		 $caseseq = explode('@',$row->casesequece_order);
		 $caseseq = implode(",",$caseseq);
		 $query1 = $this->db->query("SELECT Case_Sequence_ID, input_module FROM case_sequence WHERE Case_Sequence_ID IN ($caseseq) ORDER BY FIELD(Case_Sequence_ID,$caseseq)");
		 //echo $this->db->last_query();
		
		 return $query1->result();
	  }else{
		 $this->db->select('Main_case_sequence_ID');
		 $query = $this->db->get_where('assign_case_sequence',array('Case_ID'=>$caseid));
		 if($query->num_rows()>0){
			$row = $query->row();
			$mcaseseqid = $row->Main_case_sequence_ID;
		 }
		 
		 $this->db->order_by('Case_Sequence_ID','asc');
		 $query = $this->db->get_where('case_sequence',array('Main_case_sequence_ID'=>$mcaseseqid));
		 //echo $this->db->last_query();
		 return $query->result();
	  }
	  
   }
  
  public function getinputmodules($caseid,$partyno=''){
	  $query = $this->db->get_where('tblcasesequenceorder',array('caseid'=>$caseid));
	  if($query->num_rows()>0){
		$row = $query->row();
		 $caseseq = array();
		 $caseseq = explode('@',$row->casesequece_order);
		 $caseseq = implode(",",$caseseq);
	  }
	  if(!isset($caseseq)){
	  $query = $this->db->query("SELECT assign_case_sequence.Case_ID,assign_case_sequence.Main_case_sequence_ID, case_sequence.Case_Sequence_ID, case_sequence.input_module, case_sequence.side_authority  FROM `assign_case_sequence` JOIN case_sequence WHERE assign_case_sequence.Case_ID = '$caseid' AND case_sequence.Main_case_sequence_ID = assign_case_sequence.Main_case_sequence_ID");
	  }else{
	  $query = $this->db->query("SELECT assign_case_sequence.Case_ID,assign_case_sequence.Main_case_sequence_ID, case_sequence.Case_Sequence_ID, case_sequence.input_module, case_sequence.side_authority  FROM assign_case_sequence JOIN case_sequence WHERE assign_case_sequence.Case_ID = '$caseid' AND case_sequence.Main_case_sequence_ID = assign_case_sequence.Main_case_sequence_ID  ORDER BY FIELD(Case_Sequence_ID,$caseseq)");
	  }
	  if($partyno == ''){
		 return $query->result();
	  }else{
		 $json_respose = array();
		 foreach($query->result() as $result):
		 $node =array();
		 if($result->side_authority == '1@2' || $result->side_authority == $partyno){
			$node['Case_Sequence_ID'] = $result->Case_Sequence_ID;
			$node['input_module'] = $result->input_module;
			$node['side_authority'] = $result->side_authority;
		 }else{
			continue;
		 }
		 array_push($json_respose,$node);
		 endforeach;
		 return $json_respose;		 		 
	  }

	  //return $query->result();
	  
  }
	  public function getmodulelistbycaseseq($mcaseseqid){
		 $this->db->select('Case_Sequence_ID,input_module');
		 $query = $this->db->get_where('case_sequence',array('Main_case_sequence_ID'=>$mcaseseqid));
		 return json_encode($query->result());
	  }
  
	  public function getquesbymoduleid($moduleid){
	  $query = $this->db->query("SELECT test_alternatives.IDQuest, test_quest.Case_Sequence_ID, test_quest.note, test_quest.Question, test_alternatives.iDAlternativa, test_alternatives.option_list, test_alternatives.t_a, case_sequence.side_authority as side_auth FROM test_quest JOIN test_alternatives ON test_alternatives.IDQuest =test_quest.IDQuest JOIN case_sequence ON case_sequence.Case_Sequence_ID = test_quest.Case_Sequence_ID WHERE test_alternatives.IDQuest IN  (SELECT IDQuest FROM test_quest WHERE Case_Sequence_ID = '$moduleid')");
	  return $query->result();
	  }
	  
	  public function getquesbymodulelist($modulelist){
	  $query = $this->db->query("SELECT test_quest.IDQuest, test_quest.note, test_quest.Question, test_alternatives.option_list, test_alternatives.t_a FROM test_alternatives JOIN test_quest ON test_alternatives.IDQuest = test_quest.IDQuest WHERE test_quest.Case_Sequence_ID IN ($modulelist)");
	  return json_encode($query->result());
	  //return $this->db->last_query();
	  }
 
 
   public function getemailbyuserid($userid){
	  $query = $this->db->select('Email')->where('UserID',$userid)->get('register_user');
	  $row = $query->row();
	  if($query->num_rows() > 0){
		 $email = $row->Email;
	  }
	  return $email;
   }
 
   public function sendassignedmail($managerid,$caseid){
	  $data['managername'] = $this->getusername($managerid);
	  $data['caseid'] = $caseid;
	  $manemail = $this->getemailbyuserid($managerid);
	  $this->email->from('dev@ansitdev.com', 'CaseManagerApp');
	  $this->email->to($manemail);
	  $this->email->subject("CaseID ".$caseid. " has been assigned");
	  $mailbody = $this->load->view('email/caseassignmail', $data, true);
	  $this->email->message($mailbody);
	  if(!$this->email->send()){
		 show_error($this->email->print_debugger());
	  }
	  
   }
 
   public function sendstepmail($caseid,$userid,$moduleid){
	  $query = $this->db->query("SELECT tblcase.ManagerAssigned,register_user.FirstName,register_user.Email FROM `tblcase` JOIN register_user ON tblcase.ManagerAssigned = register_user.UserID WHERE tblcase.CaseID = '$caseid'");
	  $row  = $query->row();
	  if($query->num_rows()>0){
		 $manemail = $row->Email;
		 $managerid = $row->ManagerAssigned;
	  }
	  
	  $data['username'] = $this->getusername($userid);
	  $data['managername'] = $this->getusername($managerid);
	  $data['stepname'] = $this->getcase_sequencenamebyid($moduleid);
	  //print_r($data);
	  //echo $manemail;
	  //exit;
	  $this->email->from('dev@ansitdev.com', 'CaseManagerApp');
	  $this->email->to($manemail);
	  $this->email->subject("Client answered for step: ".$data['stepname']." in CaseID ".$caseid);
	  $mailbody = $this->load->view('email/sendstepmail', $data, true);
	  $this->email->message($mailbody);
	  if(!$this->email->send()){
		 show_error($this->email->print_debugger());
	  }
   }
 
 
   public function getusername($userid){
	  $query = $this->db->select('FirstName,LastName')->where('UserID',$userid)->get('register_user');
	  $row = $query->row();
	  if($query->num_rows() > 0){
		 $name = $row->FirstName;
	  }
	  return $name;
   
   }

   public function sendchatmail($id){
	  $this->load->library('email');
	  $this->load->helper('email');
	  $query = $this->db->get_where('message',array('MESSAGE_ID'=>$id));
	  $row = $query->row();
	  if($query->num_rows() > 0 ){
	   $txt = $row->DISCUSSION;
	   $sender = $row->RESPONCE;
	   $userid = $row->USER_ID;
	   $managerid = $row->MANAGER_ID;
	   $senderid = $row->senderid;
	   $type = $row->TYPE;
	  }
	  if($senderid == $userid){
		 $eid = $managerid;
		 $role = 1;
	  }elseif($senderid == $managerid){
		  $eid = $userid;
		  $role = 2;
	  }
	  $email = $this->getemailbyuserid($eid);
	  $data['sender'] = $sender;
	  //exit;
	  $data['msg'] = $txt;
	  $this->email->from('dev@ansitdev.com', 'CaseManagerApp');
	  $this->email->to($email);
	  $this->email->subject('Your Conversation with '.$sender);
	  $mailbody = $this->load->view('email/sendchatmsg', $data, true);
	  $this->email->message($mailbody);
	  if(!$this->email->send()){
		 show_error($this->email->print_debugger());
	  }else{
		 $this->load->model('notification_model');
		 $obj = 4;
		 $objid = $senderid;
		 $notifiedid = $eid;
		 $comment = 'New Email waiting for you from '.$sender;
		 $action = 4;
		 $this->notification_model->createnotification($eid,$obj,$objid,$notifiedid,$role,$action,$comment);
	  }
	  
   
   
   }
    public function sendintromail($insid){
	  $this->db->select('FirstName,LastName,Email,Password');
	  $this->load->library('email');
	  $this->load->helper('email');
	  $query = $this->db->get_where('register_user',array('UserID'=>$insid));
	  $row = $query->row();
	  if($query->num_rows() > 0 ){
	   $data['password'] = $row->Password;
	   $data['email'] = $row->Email;
	   $data['fn'] = $row->FirstName;
	   $data['ln'] = $row->LastName;
	  $this->email->from('dev@ansitdev.com', 'CaseManagerApp');
	  $this->email->to($data['email']);
	  $this->email->subject('Account Created for Legal Case');
	  $mailbody = $this->load->view('email/accountcreated', $data, true);
	  $this->email->message($mailbody);
	  if(!$this->email->send()){
		 show_error($this->email->print_debugger());
	  }
	  }
   }
   
   public function allowaccess($caseid,$userid){
	  $this->db->select('fpid,spid');
	  $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
	  if($query->num_rows()> 0){
		 $row = $query->row();
		 //return $row;
		 if($userid !== $row->fpid && $userid !== $row->spid){
			redirect('user_dashboard');
		 }
	  }
   }
   
   public function checkmoduleexistance(){
	  $caseseqid = $this->input->post('caseseqid', TRUE);
	  $inputmodtxt = $this->input->post('inputmodtxt',TRUE);
	  $query = $this->db->get_where('case_sequence', array('Main_case_sequence_ID'=>$caseseqid,'input_module'=>$inputmodtxt));
	  //return $this->db->last_query();
	  if($query->num_rows()>0){
		 return 0;
	  }else{
		 return 1;
	  }
	  
   }
   
   public function getmodlist(){
	  $sidetype = $this->input->post('party',TRUE);
	  $query = $this->db->get_where('case_sequence',array('side_authority'=>$sidetype));
	  if($query->num_rows()>0){
	  return json_encode($query->result());
	  }else{
		 return 0;
	  }
   }
   
   public function processinfomod(){
	  $modtype = $this->input->post('modtype',TRUE);
	  $infomodinfo = $this->input->post('infomodinfo',TRUE);
	  //return $infomodinfo;
	  $result = $this->casequesans($infomodinfo);
	  return json_encode($result);
	  
   }
   
   public function reportlist($usertype='',$userid=''){
	  //if($usertype == 1){
		 
		 //$query = $this->db->get_where('tblreport',array('IsActive'=>1));
		 $query = $this->db->query("SELECT tblreport.RepID,tblreport.ReportName,tblreport.RepoView,tblreport.NoOfColumn,tblreport.CreatedDate, case_sequence.input_module FROM tblreport JOIN case_sequence ON tblreport.case_sequence_ID = case_sequence.Case_Sequence_ID");
		 return $query->result();
	//  }else{
	//	 echo '2';
	//  }
   }
  
   public function getreportdata($repid){
	  $query = $this->db->get_where('tblreport',array('RepID'=>$repid));
	  return $query->result();
   }
   
   public function reportdata($repid){
	  $query = $this->db->query("SELECT tblreport.RepID,tblreport.ReportName,tblreport.RepoView,tblreport.NoOfColumn,tblreport.CreatedDate, case_sequence.input_module FROM tblreport JOIN case_sequence ON tblreport.case_sequence_ID = case_sequence.Case_Sequence_ID AND case_sequence.Case_Sequence_ID='$repid'");
		 return $query->result();
		 //return $this->db->last_query();
   }
   public function addreport($userid,$repid=''){
	  $reponame = $this->input->post('reponame',TRUE);
	  $roles = $this->input->post('side_authority');
	  $side = implode('@',$roles);
	  $no_col = $this->input->post('no_col',TRUE);
	  $desc = $this->input->post('repodesc',TRUE);
	  $firstcol = $this->input->post('firstcol');
	  $secondcol =  $this->input->post('secondcol');
	  $thirdcol =  $this->input->post('hiddenthirdcol');
	  //print_r($thirdcol);
	  //exit;
	  $case_sequence_id = $this->input->post('modname');
	  $fcolhead = $this->input->post('fcolhead',TRUE);
	  $secolhead = $this->input->post('secolhead',TRUE);
	  $thirdcolhead = $this->input->post('thirdcolhead',TRUE);
	  if($repid == ''){
		 $data = array('ReportName'=>$reponame,'RepoDesc'=>$desc,'RepoView'=>$side,'NoOfColumn'=>$no_col,'firstcol'=>$firstcol,'secondcol'=>$secondcol,'thirdcol'=>$thirdcol,'case_sequence_ID'=>$case_sequence_id,'fcolhead'=>$fcolhead,'secolhead'=>$secolhead,'thirdcolhead'=>$thirdcolhead,'CreatedBy'=>$userid,'CreatedDate'=>date('Y-m-d H:i:s'));
		 $this->db->insert('tblreport',$data);
	  }else{
		 $data = array('ReportName'=>$reponame,'RepoDesc'=>$desc,'RepoView'=>$side,'NoOfColumn'=>$no_col,'firstcol'=>$firstcol,'secondcol'=>$secondcol,'thirdcol'=>$thirdcol,'case_sequence_ID'=>$case_sequence_id,'fcolhead'=>$fcolhead,'secolhead'=>$secolhead,'thirdcolhead'=>$thirdcolhead,'UpdatedBy'=>$userid,'UpdatedDate'=>date('Y-m-d H:i:s'));
		 $this->db->update('tblreport',$data,array('RepID'=>$repid));
	  }

	  if($this->db->affected_rows() > 0){
		 return 1;
	  }else{
		 return 0;
	  }
   }
   
   public function getcommentbymoduleid($ModuleID){
	  $query = $this->db->query("SELECT assign_case_sequence.Case_ID FROM `assign_case_sequence` JOIN case_sequence ON assign_case_sequence.Main_case_sequence_ID =  case_sequence.Main_case_sequence_ID AND case_sequence.Case_Sequence_ID ='$ModuleID'");
	  //echo $this->db->last_query();
	  //exit;
	  foreach($query->result() as $result){
		 $row = $this->getcommentbycaseid($result->Case_ID);
	  }
	  
	  echo json_encode($row);
	  
	  
   }
   public function getcommentbycaseid($caseid){
	  $this->db->select('comment');
	  $query = $this->db->get_where('tab_comment',array('caseid'=>$caseid));
	  return $query->result();
   }
   
   public function getcase_sequencenamebyid($id){
	  $this->db->select('input_module');
	  $query = $this->db->get_where('case_sequence',array('Case_Sequence_ID'=>$id));
	  if($query->num_rows()>0){
		 $row = $query->row();
		 return $row->input_module;
	  }
   }
   
   public function showrepid($case_sequence_ID,$caseid,$userid,$utype){
	  if($utype == '2'){
		 $sideno = '3';
	  }elseif($utype == '3'){
		 $sideno = $this->getpartybyuserid($caseid,$userid);
	  }
		 $this->db->select('RepID,RepoView');
		 $query = $this->db->from('tblreport')->where('case_sequence_ID',$case_sequence_ID)->like('RepoView',$sideno)->get();
		 $json_array = array();
	
		 foreach($query->result() as $result){
			$node = array();
			$node['RepID'] = $result->RepID;
			$node['RepoView'] = $this->viewuser($result->RepoView,$sideno);
			array_push($json_array,$node);
		 }
	  echo json_encode($json_array);
	  
   }
   
   public function viewuser($repoview,$sideno){
	  if($sideno !== '3'){
		 if(strcmp($repoview,$sideno) == 0){
			return 'My Report';
		 }elseif(strcmp($repoview,"1@2") == 0){
			return 'Both Parties Report';
		 }elseif((strcmp($repoview,"1@3") == 0)||(strcmp($repoview,'2@3') == 0)){
			return 'My & Managers view';
		 }elseif(strcmp($repoview,"1@2@3") == 0){
			return 'All Parties Report';
		 }
	   }else{
		 if(strcmp($repoview,$sideno) == 0){
			return 'My Report';
		 }elseif((strcmp($repoview,"1@3") == 0)||(strcmp($repoview,"2@3") == 0)){
			return 'My and party report';
		 }elseif(strcmp($repoview,"1@2@3") == 0){
			return 'All Parties Report';
		 }
	   }
   }
   
   public function findduplicate(){
	  $side = $this->input->post('side');
	  $modid = $this->input->post('modid');
	  $repid = $this->input->post('hidrepid');
	  $query = $this->db->get_where('tblreport',array('case_sequence_ID'=>$modid,'RepoView'=>$side));
	  $result = $query->result();
	  if($repid == ''){
		 if($query->num_rows()>0){echo 0;}else{echo 1;}
	  }else{
		 if($repid == $result[0]->RepID){
			echo 1;
		 }else{
			echo 0;
		 }
	  }
   }
   
   public function findreponame(){
	  $reponame = $this->input->post('reponame');
	  $query = $this->db->get_where('tblreport',array('ReportName'=>$reponame));
	  if($query->num_rows()>0){
		 return 1;
	  }else{
		 return 0;
	  }
   }
   
   public function showcasesequenceorder($mcaseseqid,$caseid){
	  $query = $this->db->get_where('tblcasesequenceorder',array('caseid'=>$caseid,'mcaseseqid'=>$mcaseseqid));
	  if($query->num_rows()>0){
		 $row = $query->row();
		 $caseseq = array();
		 $caseseq = explode('@',$row->casesequece_order);
		 $caseseq = implode(",",$caseseq);
		 //$this->db->select('Case_Sequence_ID,input_module');
		 //$this->db->where_in('Case_Sequence_ID', $caseseq);
		 //$query1 = $this->db->get('case_sequence');
		 $query1 = $this->db->query("SELECT Case_Sequence_ID, input_module,side_authority FROM case_sequence WHERE Case_Sequence_ID IN ($caseseq) ORDER BY FIELD(Case_Sequence_ID,$caseseq)");
		 //echo $this->db->last_query();
		
		 echo json_encode($query1->result());
	  }else{
		 $this->db->order_by('Case_Sequence_ID','asc');
		 $query = $this->db->get_where('case_sequence',array('Main_case_sequence_ID'=>$mcaseseqid));
		 //echo $this->db->last_query();
		 echo json_encode($query->result());
	  }
	  
   }
   
   public function chgcasesequenceorder($userid){
	  $mcaseseqid = $this->input->post('mcaseseqid',TRUE);
	  $caseid = $this->input->post('caseid',TRUE);
	  $DataList = $this->input->post('DataList',TRUE);
	  $query = $this->db->get_where('tblcasesequenceorder',array('caseid'=>$caseid,'mcaseseqid'=>$mcaseseqid));
	  if($query->num_rows()>0){
		 $data = array('casesequece_order'=>$DataList,'updated_by'=>$userid,'updated_on'=>date('Y-m-d h:i:s'));
		 $this->db->update('tblcasesequenceorder',$data,array('caseid'=>$caseid,'mcaseseqid'=>$mcaseseqid));
		 if($this->db->affected_rows() > 0){
			$res =  2;
		 }else{
			$res = 0;
		 }
	  }else{
		 $data = array('caseid'=>$caseid,'mcaseseqid'=>$mcaseseqid,'casesequece_order'=>$DataList,'created_by'=>$userid,'created_on'=>date('Y-m-d h:i:s'));
		 $this->db->insert('tblcasesequenceorder',$data);
		 if($this->db->affected_rows() > 0){
			$res = 1;
		 }else{
			$res = 0;
		 }
	  }
	  $this->chgtblcasesteps($mcaseseqid,$caseid,$DataList);
	  return $res;
   }
   
   public function chgtblcasesteps($mcaseseqid,$caseid,$DataList){
	//  $this->db->select('fpid,spid');
	//  $query = $this->db->get_where('tblcase',array('CaseID'=>$caseid));
	//  $row = $query->row();
	//  if($query->num_rows()>0){
	//	$fpid = $row->fpid;
	//	$spid = $row->spid;
	//  }
	  $stepo = explode('@',$DataList);
	  foreach($stepo as $step){
		 $partyno = $this->getsidebycaseidmaincaseseq($step,$mcaseseqid);
		 $data = array('party'=>$partyno,'caseid'=>$caseid,'caseseqid'=>$step,'orderby'=>$DataList,'created_on'=>date('Y-m-d h:i:s'));
		 $this->db->insert('tblcasesteps',$data);
	  }
   }
 
  function getsidebycaseidmaincaseseq($caseseqid,$mcaseseqid){
			   $query = $this->db->select('side_authority')->where('Case_Sequence_ID',$caseseqid)->where('Main_case_sequence_ID',$mcaseseqid)->get('case_sequence');
			   if($query->num_rows()>0){
				$row = $query->row();
				return $row->side_authority;
			   }
			}
   
   public function setcasesequenstatus($mcaseseqid,$caseid){
	  $caseorderarr = array();
	  $caseorderarr = $this->getinputmodules($caseid);
	  print_r($caseorderarr);
	  foreach($caseorderarr as $caseorder){
		 $row = array();
		 $row['caseid'] = $caseorder->Case_ID;
		 $row['input_module'] = $caseorder->input_module;
		 $row['Case_Sequence_ID'] = $caseorder->Case_Sequence_ID;
		 //$row['pause'] = 
		 
	  }


	//  $query = $this->db->get_where('tblcasesequenceorder',array('mcaseseqid'=>$mcaseseqid,'caseid'=>$caseid));
	//  $row = $query->row();
	//  if($query->num_rows()>0){
	//	$datalist =  $row->casesequece_order;
	//  }
	//  $query = $this->db->get_where('tblcasesteps',array('caseid'=>$caseid,'orderby'=>$datalist));
	//  $node = array();
	//  foreach($query->result() as $step){
	//	 $row = array();
	//	 $row['caseid'] = $caseid;
	//	 $row['userid'] = $step->userid;
	//	 $row['caseseqid'] = $step->caseseqid;
	//	 $row['caseseqname'] = $this->getmodulenamebycaseseqid($step->caseseqid);
	//	 $row['reviewed'] = $step->reviewed;
	//	 $row['pause'] = $step->pause;
	//	 $row['continue_next'] = $step->continue_next;
	//	 $row['status'] = $step->status;
	//	 array_push($node,$row);
	//  }
	//  echo json_encode($node);
   }
   
   public function setaseseqorder($mcaseseqid,$caseid){
	  $query = $this->db->get_where('tblcasesequenceorder',array('mcaseseqid'=>$mcaseseqid,'caseid'=>$caseid));
	  $row = $query->row();
	  //echo json_encode($row);
	  //exit;
	  if($query->num_rows()>0){
		$datalist =  $row->casesequece_order;
	  }
	  $query = $this->db->get_where('tblcasesteps',array('caseid'=>$caseid,'orderby'=>$datalist));
	  //echo $this->db->last_query();
	  //echo json_encode($query->result());
	  //exit;
	  $node = array();
	  foreach($query->result() as $step){
		 $row = array();
		 $row['caseid'] = $caseid;
		 $row['partyno'] = $step->party;
		 $row['caseseqid'] = $step->caseseqid;
		 $row['caseseqname'] = $this->getmodulenamebycaseseqid($step->caseseqid);
		 $row['reviewed'] = $step->reviewed;
		 $row['pause'] = $step->pause;
		 $row['continue_next'] = $step->continue_next;
		 $row['status'] = $step->status;
		 array_push($node,$row);
	  }
	  echo json_encode($node);
   }

   public function sendfollowupmail($caseid,$inscaseseqid){
	  $stepname = $this->getmodulenamebycaseseqid($inscaseseqid);
	  
   
   
   }
   
   public function stepactivationmail($userid,$caseid,$stepname){
		 $this->db->select('FirstName,Email');
		 $query = $this->db->get_where('register_user',array('UserID'=>$userid));
		 $row = $query->row();
		 if($query->num_rows() > 0 ){
		  $data['caseid'] = $caseid;
		  $data['email'] = $row->Email;
		  $data['fn'] = $row->FirstName;
		 $data['stepname'] = $stepname;
		 $this->email->from('dev@ansitdev.com', 'CaseManagerApp');
		 $this->email->to($data['email']);
		 $this->email->subject('Step '.$stepname.' activated for your case:'.$caseid);
		 $mailbody = $this->load->view('email/stepactivationmail', $data, true);
		 $this->email->message($mailbody);
		 if(!$this->email->send()){
			show_error($this->email->print_debugger());
		 }else{
			echo "mail sent".$data['email'];
		 }
	  }

   }
   public function updatecaseseqstat($caseseqid,$caseid,$userid,$type,$val){
	  $data = array();
	  if($type == 'pause' and $val == 1){
		 $data = array('pause'=>1,'continue_next'=>0);
	  }elseif($type == 'continue_next' and $val == 1){
		 $data = array('pause'=>0,'continue_next'=>1);
	  }else{
		$data = array($type=>$val); 
	  }
	  
	  $this->db->update('tblcasesteps',$data,array('caseseqid'=>$caseseqid,'caseid'=>$caseid,'party'=>$userid));
	  if(($type == 'status') && ($val == 1)){
		  $stepname = $this->getcase_sequencenamebyid($caseseqid);
		 $this->stepactivationmail($userid,$caseid,$stepname);
	  }

   }
   
   public function chkcaseseqstatus($caseid,$caseseqid){
	  $query = $this->db->get_where('tblcasesteps',array('caseid'=>$caseid,'caseseqid'=>$caseseqid));
	  return $query->row();
	  
   }

   
   public function preprocessnextstep($caseid,$userid,$moduleid){
	  //$query = $this->db->get_where('tblcasesteps',array('userid'=>$userid,'caseid'=>$caseid,'caseseqid'=>$moduleid));
	  $query = $this->db->get_where('tblcasesteps',array('caseid'=>$caseid,'caseseqid'=>$moduleid));
	  $row = $query->row();
	  if($query->num_rows() >0 ){
		 $orderby = $row->orderby;
		 $ordlis = explode('@',$orderby);
		 $key =  array_search($moduleid, $ordlis);
		 $remarr =  array_slice($ordlis,$key+1);
		 if(isset($remarr[0])){
			$nextcaseseqid =  $remarr[0];
		 }else{
			return "lstep";
		 }
	  }
	  
	  $query = $this->db->get_where('tblcasesteps',array('caseid'=>$caseid,'caseseqid'=>$nextcaseseqid));
	  if($query->num_rows() >0 ){
		 $row =  $query->row();
		 $contnxt = $row->continue_next;
		 $active = $row->status;
		 $pause = $row->pause;
		 if($contnxt == 1){
			//$this->db->update('tblcasesteps',array('status'=>1),array('userid'=>$userid,'caseid'=>$caseid,'caseseqid'=>$nextcaseseqid));
			$this->db->update('tblcasesteps',array('status'=>1),array('caseid'=>$caseid,'caseseqid'=>$nextcaseseqid));
			return 1;
		 }else{
			$this->db->update('tblcasesteps',array('status'=>0),array('caseid'=>$caseid,'caseseqid'=>$nextcaseseqid));
			return 2;
		 }
	  }
	  
   }

   public function getmodulenamebycaseseqid($id){
	  $query = $this->db->get_where('case_sequence',array('Case_Sequence_ID'=>$id));
	   $row = $query->row();
	  if($query->num_rows()>0){
		
		 return $row->input_module;
	  }
   
   }
}?>