<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">
	
    <!-- Casestyle CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">

   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/vendor/validate/validetta.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
   <script type="text/javascript" src="<?php echo base_url(); ?>media/assets/js/jquery-1.10.2.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>media/vendor/editors/ckeditor/ckeditor.js"></script>
</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">
       <?php echo $common_header;?>

       <?php echo $right_panel; ?>
     
       
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"><?php echo isset($allreport[0])?'Edit':'Add';?> Report</a>
				</li>
			</ol>
		</div>
		
	   
	</header>
	
	<!-- End: Topbar -->

    <!-- Begin: Content -->
   
     <div id="content" class="animated fadeIn">
        <div class="row">
      
			

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> <?php echo isset($allreport[0])?'Edit':'Add';?> Report
							</span>
                        </div>
						<?php //print_r(isset($allreport)?$allreport:'')?>
						<br>
						<?php $attributes = array('class'=>'reportedit');
						if(isset($allreport[0]->RepID)){
						   echo form_open("case_sequence/editreport/".$allreport[0]->RepID,$attributes);
						}else{
						echo form_open("case_sequence/addreport",$attributes);   
						}?>
						
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div>
								<div class="admin-form">
								 
									<div class="form-group">
									<label class="col-lg-2 control-label" for="inputStandard"> Report Name :</label>
									<div class="col-lg-6">
									    <input type="text" class="form-control" id="reponame" name="reponame" value="<?php echo isset($allreport[0]->ReportName)?$allreport[0]->ReportName:"";?>" data-validetta="required"/>
										<span class="repoerr red"></span>
									</div>
									<div class="col-lg-12"><br></div>
									</div>
								 <div class="col-lg-12"><br></div>
									<div class="form-group">
									<label class="col-lg-2 control-label" for="inputStandard"> User role  :</label>
									<div class="col-lg-6">
										<?php if(isset($allreport[0]->RepoView)){$data = explode('@',$allreport[0]->RepoView);}?>
										<select name="side_authority[]" class="form-control" id="role_name" multiple="multiple"  data-validetta="required">
									   <?php if(isset($data[0])){
										 if (in_array(1, $data)){$var1 = 'selected';}
										 if (in_array(2, $data)){$var2 = 'selected';}
										 if (in_array(3, $data)){$var3 = 'selected';}
									   }?>
										<option value="1" <?php echo isset($var1)?$var1:'';?>>  Side 1 </option>
										<option value="2"<?php echo isset($var2)?$var2:'';?>>  Side 2 </option>
										<option value="3"<?php echo isset($var3)?$var3:'';?>>  Manager </option>
									  
										</select>
									</div>
									<div class="col-lg-12"><br></div>
									</div>
									<div class="form-group">
									<label class="col-lg-2 control-label" for="inputStandard"> Report Description:</label>
									<div class="col-lg-6">
									   <textarea class="gui-textarea" name="repodesc" id="form-field-8" data-validetta="required"><?php echo isset($allreport[0]->RepoDesc)?$allreport[0]->RepoDesc:''; ?></textarea>
									</div>
									   <div class="col-lg-12"><br></div>
									</div>									
									<div class="form-group">
									<label class="col-lg-2 control-label" for="modname"> Module Name:</label>
									<div class="col-lg-6">
									<label class="field select">
										<select id="modname" name="modname" class="form-control model" data-validetta="required">
										<!--<option value="" <?php //echo (isset($allreport[0]->NoOfColumn)&&($allreport[0]->NoOfColumn == '1'))?'selected':'';?>>1</option>-->
										<option value="">--Select Module Name--</option>
										<?php if(isset($allreport[0]->case_sequence_ID)){
										  foreach($getallmodule as $getallmodule){?>
										  <option value="<?php echo $getallmodule->Case_Sequence_ID?>" <?php echo ($getallmodule->Case_Sequence_ID == $allreport[0]->case_sequence_ID)?"selected":""?>><?php echo $getallmodule->input_module?></option>
										  <?php }
										 }else{foreach($getallmodule as $getallmodule){
										  echo '<option value="'.$getallmodule->Case_Sequence_ID.'">'.$getallmodule->input_module.'</option>';
										} }?>
										
										
											   
										</select>
										<i class="arrow double"></i>
									</label>



									</div>
									   <div class="col-lg-12"><br></div>
									</div>									

									<div class="col-lg-12"><br></div>
									<div class="form-group">
									<label class="col-lg-2 control-label" for="no_col"> No Of Columns:</label>
									<div class="col-lg-6">
									<label class="field select">
										<select id="no_col" name="no_col" required class="form-control model">
										<option value="1" <?php echo (isset($allreport[0]->NoOfColumn)&&($allreport[0]->NoOfColumn == '1'))?'selected':'';?>>1</option>
										<option value="2" <?php echo (isset($allreport[0]->NoOfColumn)&&($allreport[0]->NoOfColumn == '2'))?'selected':'';?>>2</option>
										<option value="3" <?php echo (isset($allreport[0]->NoOfColumn)&&($allreport[0]->NoOfColumn == '3'))?'selected':'';?>>3</option>
											   
										</select>
										<i class="arrow double"></i>
									</label>



									</div>
									   <div class="col-lg-12"><br></div>
									</div>									
									<div class="form-group">
									<label class="col-lg-2 control-label" for="fcolhead"> First Column Heading :</label>
									<div class="col-lg-6">
									    <input type="text" class="form-control" name="fcolhead" value="<?php echo isset($allreport[0]->fcolhead)?$allreport[0]->fcolhead:"";?>"/>
									</div>
									<div class="col-lg-12"><br></div> 
									<label class="col-lg-2 control-label" for="inputStandard"> First Column:</label>
									<div class="col-lg-6">
									   <label><input type="radio" name="firsteditrad" value="1">Agree</label>
									   <label><input type="radio" name="firsteditrad" value="0">Disagree</label>
									   
									    <textarea id="firstcol" name="firstcol" class="ckeditor"><?php echo isset($allreport[0]->firstcol)?$allreport[0]->firstcol:''; ?></textarea>
									</div>
									   <div class="col-lg-12"><br></div>
									</div>									
									<div class="form-group seccoldiv">
									<label class="col-lg-2 control-label" for="secolhead"> Second Column Heading :</label>
									<div class="col-lg-6">
									    <input type="text" class="form-control" name="secolhead" value="<?php echo isset($allreport[0]->secolhead)?$allreport[0]->secolhead:"";?>"/>
									</div>
									<div class="col-lg-12"><br></div>    
									<label class="col-lg-2 control-label" for="inputStandard"> Second Column:</label>
									<div class="col-lg-6">
									   <label><input type="radio" name="seceditrad" value="1">Agree</label>
									   <label><input type="radio" name="seceditrad" value="0">Disagree</label>
									    <textarea id="secondcol" name="secondcol" class="ckeditor"><?php echo isset($allreport[0]->secondcol)?$allreport[0]->secondcol:''; ?></textarea>
									</div>
									   <div class="col-lg-12"><br></div>
									</div>									
									<div class="form-group thirdcoldiv">
									   <label class="col-lg-2 control-label" for="secolhead"> Third Column Heading :</label>
									   <div class="col-lg-6">
									    <input type="text" class="form-control" name="thirdcolhead" value="<?php echo isset($allreport[0]->thirdcolhead)?$allreport[0]->thirdcolhead:'';?>"/>
									   </div>
									<div class="col-lg-12"><br></div>       
									<label class="col-lg-2 control-label" for="inputStandard"> Third Column:</label>
									<div class="col-lg-6">
									   <!--<textarea id="thirdcol" name="thirdcol" class="ckeditor"></textarea>-->
									   <div id="thirdcol" class="gui-textarea"><?php echo isset($allreport[0]->thirdcol)?$allreport[0]->thirdcol:'<ul></ul>'; ?></span></div>
									  <textarea id="hiddenthirdcol" name="hiddenthirdcol" style="visibility: hidden;"></textarea>
									</div>
									   <div class="col-lg-12"><br></div>
									</div>									


								</div>	
								<div class="col-lg-12"><br></div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-8">
								<button id="butsub" class="btn active btn-success" type="submit" name="test">
								<i class="fa fa-save"></i>  Submit </button> 								
									<button class="btn active btn-warning " type="button" onclick="javascript:window.history.back();">
									<i class="fa fa-warning"></i> Cancel </button>
								</div>	
								</div>
							</div>
							<input type="hidden" id="hidrepid" name="hidrepid" value="<?php echo isset($allreport[0]->RepID)?$allreport[0]->RepID:''?>"/>

							
						</div><!-- end col-md-12 -->
						<?php echo form_close();?>
		 
					</div>
				</div>
        </div>
	</div>
            <!-- End: Content -->  
</section>
<?php //echo implode('@',$this->input->post('side_authority'));?>
<script type="text/javascript" src="<?php echo base_url(); ?>media/vendor/validate/validetta.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#role_name').multiselect();
	
	$(".reportedit").validetta({
	   realTime : true,
	    display : 'inline',
        errorTemplateClass : 'validetta-inline'
	  });	
	
    });
	
    </script>
<script>
   function validmod() {
		 if (($('#role_name').val() !== '')&& ($('#modname').val() !== '')) {
		 //var side = $('#role_name').val().toString();
		 var sides = $('#role_name').val().toString();
		 side = sides.replace(/,/g,'@');
		  var modid= $('#modname').val();
		  var repid = $('#hidrepid').val();
		  data= {'side':side,'modid':modid,'repid':repid};
		  url = "<?php echo base_url().'case_sequence/findduplicate'?>";
		  console.log(data);
		 $.post(url,data,function(output){
			console.log(output);
			if (output == '1') {
			   isvalid = '2';
			}else{
			   isvalid = '1';
			}
		 });
		 }
   }
   $(document).ready(function(){
	  isvalid = false;
	  orgreponame =  $('#reponame').val();
	  $('#role_name').on('change',function(){
		 validmod();
	  });
	  $('#modname').on('change',function(){
		 validmod();
	  });
	  $('.reportedit').on('submit',function(e){
		 $this = $(this);
		 if(isvalid == '1'){
		 e.preventDefault();
		 alert('Roles and Module Name are already exist');
		 }else if (isvalid == '2') {
			   $this.submit();
		 }

       });
	  $('#reponame').on('blur',function(){
		 if (orgreponame !== $('#reponame').val()) {
			$.post('<?php echo base_url().'case_sequence/checkreponame'?>',{'reponame':$(this).val()},function(data){
			   console.log(data);
			   if (data == 1) {
				  $('.repoerr').html('Report already with this name');
				  $('#reponame').focus();
			   }else{
				  $('.repoerr').html('');
			   }
			});			
		 }else{
			$('.repoerr').html('');
		 }

	  });
   });
</script>


<script>
   function editorvisi(nocol) {
	  $('#thirdcol').empty();
	  if (nocol == 1) {
		 $('.seccoldiv').hide();
		 $('.thirdcoldiv').hide();
	  }else if (nocol == 2) {
		 $('.seccoldiv').show();
		 $('.thirdcoldiv').hide();
	  }else{
		 if (nocol == 3){
			var modid = $('#modname').val();
			$.post('<?php echo base_url().'case_sequence/getcommentbymoduleid/'?>'+modid,function(data){
			   obj = $.parseJSON(data);
			   console.log(obj);
			    //$('#thirdcol').append('<ul>');
			   $.each(obj, function(i,v){
				$('#thirdcol').append('<li>'+obj[i].comment+'</li>');
				$('#hiddenthirdcol').val($('#hiddenthirdcol').val()+'<li>'+obj[i].comment+'</li>');
			   });
			   //$('#thirdcol').append('</ul>');
			    
			});
		 }
		 
		 $('.seccoldiv').show();
		 $('.thirdcoldiv').show();
	  }
   }
   
   $(document).ready(function(){
	  var nocol = $('#no_col').val();
	  editorvisi(nocol);
	 $('#no_col').on('change',function(){
	  editorvisi($(this).val());
	 });
	 
	 $('#modname').on('change',function(){
	  $('#no_col').val('1');
	 });
	 
	 $('input[name="firsteditrad"]').click(function(){
	  if ($(this).is(':checked')) {
		 radval =  $(this).val();
		 if(radval == '1') {
			CKEDITOR.instances.firstcol.insertHtml('<p><span style="background-color:#00FF00">I agree</span></p>');
		 }else{
			CKEDITOR.instances.firstcol.insertHtml('<p><span style="background-color:#FF0000">I disagree</span></p>');
		 }
	  }
	 });
    $('input[name="seceditrad"]').click(function(){
	  if ($(this).is(':checked')) {
		 radval =  $(this).val();
		 if(radval == '1') {
			CKEDITOR.instances.secondcol.insertHtml('<p><span style="background-color:#00FF00">I agree</span></p>');
		 }else{
			CKEDITOR.instances.secondcol.insertHtml('<p><span style="background-color:#FF0000">I disagree</span></p>');
		 }
	  }
	 });
 

	 
   });
</script>
        <!-- End: Content-Wrapper -->
 <?php echo $common_footer ?>       
        
        
