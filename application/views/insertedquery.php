<div class="col-md-12">   
   <div class="row form-horizontal">
		<div class="col-md-6">
			<div class="admin-form" id="frmaddmodule">

				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Question : </label>
				<div class="col-lg-8">
				<textarea class="gui-textarea" name="question[]" id="form-field-8" data-validetta="required">
				  Question is <?php echo $ques ?>&#13;&#10;
				  Answer is : <?php echo $ans?>
					 
				  
				</textarea>
				<input id="form-field-6" type="hidden" name="IDQuest[]" value="0">
				<input id="form-field-6" type="hidden" name="iDAlternativa[]" value="0">
				</div>
				</div>
			   <div class="form-group divnote">
				  <label class="col-lg-4 control-label" for="inputStandard"> Note:</label>
				  <div class="col-lg-8">
				  <span class="labnote"></span>
				  <textarea class="gui-textarea textnote hidden" name="notes[]" id="form-field-8"></textarea>
				  </div>
			   </div>

				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Response type:</label>
				<div class="col-lg-8">
				<label class="field select">
					<select class="t_a" name="t_a[]"  data-validetta="required">
						<option value="">Select any</option>
						<option value="Radio"  >Radio Group</option>
						<option value="Checkbox" >Check Box</option>
						<option value="Text" >Text Box</option>
						<option value="file">Upload</option>
					</select>
					<i class="arrow double"></i>
				</label>
				
				</div>
				<div class="option_set"></div>
				</div>
				
			</div>
		</div>
		
		<div class="col-md-6" align="right">
			<div class="admin-form">
				<div class="form-group">
				
				<div class="col-lg-5">
				<button class="btn btn-primary btn-sm removequestion"> <i class="fa fa-power-off"></i> Remove </button>&nbsp;
				<button class="btn btn-primary btn-sm addnote"  data-toggle="modal" data-target="#modalAddBrand"> <i class="fa fa-paperclip"></i> Add Notes </button>
				</div>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	</div>
	