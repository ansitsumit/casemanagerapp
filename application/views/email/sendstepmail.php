<!DOCTYPE html>

<html>
<head>
    <title>Case Manager App</title>
</head>

<body>

<h3>Hello <?php echo trim($managername)?>,</h3>
<p>User <?php echo $username?> has replied to step <?php echo $stepname.' on '.date('M-d-Y H:i:s')?>.</p>


<p>Website: <u><?php echo base_url()?></u></p>

<p>Regards,</p>
<p>Case Manager App</p>
<h6>This is system generated mail, please do not reply to this mail.</h6>
</body>
</html>
