<!DOCTYPE html>

<html>
<head>
    <title>Case Manager App</title>
</head>

<body>

<h3>Hello <?php echo trim($managername)?>,</h3>

<p>You have been  assigned a new case <?php echo $caseid .' on '.date('M-d-Y H:i:s')?>. Please check your dashboard.</p>


<p>Website: <u><?php echo base_url()?></u></p>

<p>Regards,</p>
<p>Case Manager App</p>
<h6>This is system generated mail, please do not reply to this mail.</h6>
</body>
</html>
