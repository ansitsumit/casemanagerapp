<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">
	
    <!-- Casestyle CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
   <script type="text/javascript" src="<?php echo base_url(); ?>media/assets/js/jquery-1.10.2.js"></script>


</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">
       <?php echo $common_header;?>

       <?php echo $right_panel; ?>
     
       
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">Follow Up Module</a>
				</li>
			</ol>
		</div>
		
	   
	</header>
	<div class="backbutton"><a href="javascript:window.history.back();" class="btn btn-info"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Case Page</a></div>
	<!-- End: Topbar -->

    <!-- Begin: Content -->
   
     <div id="content" class="animated fadeIn">
        <div class="row">
      <?php $this->load->model('case_model');?>
			<?php //print_r($ques);?>

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Follow Up Module
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div>
								<div class="admin-form">
	
									<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">


									<div>
									   <?php foreach($ques as $getqu){?>
									   <div class="secques">
											 <div class="form-group">
												<label class="col-lg-2 control-label" for="inputStandard"> Question :  </label>
												<div class="col-lg-8">
												<label class="gui-textarea"><?php echo $getqu->Question?></label>
												</div>
												<div class="col-lg-12"></div>
												</div>
												<div class="form-group">
												<label class="col-lg-2 control-label" for="inputStandard"> User Responded: </label>
												<div class="col-lg-8">
												   <div class="well"> 
												   <?php
												   $answtxt = $this->case_model->getsingleanswerbyidquestnuser($getqu->IDQuest,$userid);
												   //print_r($answtxt);
												   if($getqu->t_a == 'Text'){?>
												   <span class="labanswer"><?php echo isset($answtxt)?$answtxt:'';?></span>
												   <?php }elseif($getqu->t_a == 'Checkbox'){
													  $opt =  explode('@',$getqu->option_list);
													  if(isset($answtxt)){
														$valarr = explode('@', $answtxt);
													  }else{
														$valarr = array();
													  }
													  ?>
													  <span class="checkbox-custom checkbox-info labanswer">
														 <?php 
													 
													 $k = 0;
													 foreach($opt as $opt):
													 if($opt !== ''){
													   $k++;
													 ?>
													   <input type="checkbox" class="chkgen<?php echo $getqu->IDQuest?>" value="<?php echo $opt;?>"  <?php echo in_array($opt, $valarr)?"checked":"" ?> >
													   <label for="ck<?php echo $getqu->IDQuest.'q'.$k?>"><?php echo $opt;?></label>
												   <?php }endforeach;?>
													  </span>
												   <?php }elseif($getqu->t_a == "Radio"){
													  echo '<span class="labanswer">';
													  echo '<div class="radio-custom radio-primary">';
										  //echo '<select class="form-control" name=anstxt[]>';
													  $opt =  explode('@',$getqu->option_list);
													  $j=0;
													  foreach($opt as $opt):
													  if($opt !== ''){
														$j++;
													  ?>
													  <input type="radio" id="rad<?php echo $getqu->IDQuest.$j?>" class="ansrad" name="ansrad<?php echo $getqu->IDQuest?>" value="<?php echo $opt?>" <?php if(isset($answtxt)){ echo ($answtxt == $opt)?"checked":"";}?> disabled="disabled">
													  <label for="rad<?php echo $getqu->IDQuest.$j?>"><?php echo $opt?></label>
													  <?php
													  }
													  endforeach;
													  echo '</div></span>';
													  }elseif($getqu->t_a == "file"){?>
													  <span class="labanswer">
														 <div class="divfiles">
														 <?php
														 //echo $answtxt;
														  $files = $this->case_model->getfilesbyref($answtxt);
														  //print_r($files);
														  foreach($files as $file):
														  if (preg_match('/image/',$file->f_type)) {
														  ?>
														  <label>
															 <a href="<?php echo site_url('uploads/'.$file->f_name)?>" data-lightbox="image-<?php echo $i-1?>" class="smthumbnail file" ><img src="<?php echo site_url('uploads/'.$file->f_name)?>"/></a>
														 </label>
														  
													 <?php }elseif(preg_match('/pdf/',$file->f_type)){
													   echo '<label style="margin-top:15px;"><a class="file" href="'.base_url().'uploads/'.$file->f_name.'" target="_blank"><i class="fa fa-file-pdf-o fa-5x"></i></a></label>';
													 }elseif(preg_match('/document/',$file->f_type)){
													   echo '<label style="margin-top:15px;"><a class="file" href="'.base_url().'uploads/'.$file->f_name.'" target="_blank"><i class="fa fa-file-text-o fa-5x"></i></a></label>';
													 } endforeach; ?>
													 </div></span>
													 <?php }?>
												   </div>
												   <button class="btn btn-primary btn-sm insertaddedquestion" data-idquest="<?php echo $getqu->IDQuest?>" data-type="<?=$getqu->t_a?>" type="button"> <i class="fa fa-plus"></i> Insert As Query </button>
												<div class="col-lg-12"><br></div>
												
												</div>
												<div class="col-lg-12"><br></div>
												
									</div>
											 <?php }?>
									</div>
									<div class="addnewquestion"></div>
									

								</div>	
								<div class="col-lg-12"><br></div>
							</div>

							
						</div><!-- end col-md-12 -->

						 <div>
						   <div class="col-md-12" style="background-color: #fff; padding-bottom: 10px; margin-top: -1px;">
								<div class="form-group">
								<label class="col-lg-2 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-10">
								 <button class="btn active btn-primary" name="addmodule" type="submit"> 
								<i class="fa fa-plus"></i>  Submit </button>
								 <button class="btn active btn-success new_question" type="button"> 
								<i class="fa fa-plus"></i>  Add More </button>
<!--								<button class="btn active btn-success" type="button" data-toggle="modal" data-target="#addquesmod"> 
								<i class="fa fa-plus"></i>  Add More </button>
-->								<button class="btn active btn-warning " type="button" onclick="javascript:window.history.back()">
					
									<i class="fa fa-warning"></i> Cancel </button>
								</div>	
								</div>
							</div>
						   </form>
						   <!--<div class="col-md-6" style="background-color: #ffffff;">&nbsp;</div>-->
						 </div>

					</div>
				</div>
        </div>
	</div>
            <!-- End: Content -->  
</section>


<div id="addquesmod" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Question</h4>
      </div>
      <div class="modal-body">
		<div>
			<div class="admin-form">
			   <form action="<?php echo base_url().'case_sequence/addques/' ?>" method="post" class="addquesfrm">
				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Question : </label>
				<div class="col-lg-8">
				<textarea class="gui-textarea" name="question" id="form-field-8"></textarea>
				<span class="errtxt red"></span>
				</div>
				</div>
				<p>&nbsp;</p>
				<?php echo form_hidden('moduleid')?>
				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Response type:</label>
				<div class="col-lg-8">
				<label class="field select">
					<select class="t_a" name="t_a" id="selta">
						<option value="" selected>Select any</option>
						<option value="Radio">Radio Group</option>
						<option value="Checkbox" >Check Box</option>
						<option value="Text" >Text Box</option>
						<option value="file">Upload</option>
					</select>
					<i class="arrow double"></i>
				</label>
				<span class="errsel red"></span>
				
				</div>
				<div class="option_set"></div>
				</div>
				<div>
				  <p>&nbsp;</p>
				 <span class="centeralbutton"> 
				  <button class="btn active btn-success" type="submit"> 
								<i class="fa fa-plus"></i>  Add </button>
				 <button class="btn active btn-warning " type="button" data-dismiss="modal">Cancel</button>
				</span>
				</div>
			   </form>				
			</div>
		</div>
	  
	  </div>
      <div class="modal-footer">
        <span class="errmsg red pull-left"></span>
      </div>
    </div>

  </div>
</div>

<script>
   $(document).ready(function(){
	  $('.buttonpanel').hide();
	  $('#form-field-8').keypress(function(){
		 $('.errtxt').html('');
	  });
	  
	  $('#form-field-8').on('blur',function(){
	  if ($(this).val() == '') {
		 $('.errtxt').html('Please fill the question');
		}
	  });
	  
	  $('#selta').on('change',function(){
		 $('#selta option:first' ).hide();
		 $('.errsel').html('');
	  });
   
   })
</script>

        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>       
        
        
