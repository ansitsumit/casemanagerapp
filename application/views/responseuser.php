<?php $this->load->model('case_model'); ?>
<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
   <!-- <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
-->
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/vendor/validate/validetta.min.css">
	<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/css/jqvalidate/screen.css">-->

	    <!-- casestyle CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">

   <!--lightbox-->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lightbox.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
   <!--<script type="text/javascript" src="<?php echo base_url(); ?>media/assets/js/jquery-1.10.2.js"></script>-->
   
 
</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">

       <?php echo $common_header;?>

       <?php echo $right_panel; ?>
     
       
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">Module</a>
				</li>
			</ol>
		</div>
		
	   
	</header>
	
	<!-- End: Topbar -->

    <!-- Begin: Content -->
   
    <?php /*error_reporting(0);*/?>
     <div id="content" class="animated fadeIn">
        <div class="row">
      
			
            <form id="frmaddrespon" class="form-horizontal" method="post" action="" enctype="multipart/form-data">

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12"></div>
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Answer Module
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div class="errorLabelContainer"></div>
							<?php //print_r($getques);?>

							<?php
							$i = 0;
							foreach($getques as $getqu):
							
								  $i++; 
							
							?>
							<div class="col-md-6">
								<div class="admin-form">
								
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> Question <?php echo $i;?>:  </label>
									<div class="col-lg-8">
									<label class="gui-textarea"><?php echo $getqu->Question?></label>
									<input  type="hidden" name="IDQuest[]" value="<?php echo $getqu->IDQuest?>">
									<input  type="hidden" name="iDAlternativa[]" value="<?php echo $getqu->iDAlternativa?>">
									<input type="hidden" name="option_list[]" value="<?php echo $getqu->option_list?>"/>
									<input type="hidden" name="ta[]" value="<?php echo $getqu->t_a?>">
									</div>
									</div>
									<?php if(($getqu->note !== Null) && ($getqu->note !== '')){?>
									<label class="col-md-4 control-label" for="inputStandard"> Note : </label>
									<div class="col-md-8">
									<label class="notelabel"><?php echo $getqu->note?></label>
									<p>&nbsp;</p>
									</div>
									
									<?php }?>
									<div class="form-group">
									<label class="col-lg-4 control-label" for="inputStandard"> Respond Please: </label>
									<div class="col-lg-8">
									<?php if(isset($filledans[$i-1]['saveanswer']) && (!is_null($filledans[$i-1]['saveanswer']) || ($filledans[$i-1]['saveanswer']) !== '')){?>
									  <button class="btn btn-primary btn-sm adddraftedanswer" data-ques="<?php echo $getqu->IDQuest?>" data-caseid="<?php echo $caseid?>"> <i class="fa fa-paperclip"></i> Add Draft answer <?php $filledans[$i-1]['saveanswer']?> </button>
									<?php }?>
									
								  

								  
										<?php if($getqu->t_a == "Text"){?>
										  <input type="text" class="form-control" name="anstxt<?php echo $getqu->IDQuest?>[]" value="<?php echo isset($filledans[$i-1]['anstxt'])?$filledans[$i-1]['anstxt']:"";?>"data-validetta="required"/>
										<?php }elseif($getqu->t_a == "Radio"){
										  echo '<div class="radio-custom radio-primary">';
										  //echo '<select class="form-control" name=anstxt[]>';
										  $opt =  explode('@',$getqu->option_list);
										  $j=0;
										foreach($opt as $opt):
										if($opt !== ''){
										  $j++;
										?>
										<input type="radio" id="rad<?php echo $getqu->IDQuest.$j?>" name="ansrad<?php echo $getqu->IDQuest?>" value="<?php echo $opt?>" <?php if(isset($filledans[$i-1]['anstxt'])){ echo ($filledans[$i-1]['anstxt'] == $opt)?"checked":"";}?> data-validetta="required">
										<label for="rad<?php echo $getqu->IDQuest.$j?>"><?php echo $opt?></label>
										<?php
										}
										endforeach;
										echo '</div>';
										}elseif($getqu->t_a == "Checkbox"){
										  $opt =  explode('@',$getqu->option_list);
										  if(isset($filledans[$i-1]['anstxt'])){
											$valarr = explode('@', $filledans[$i-1]['anstxt']);
										  }else{
											$valarr = array();
										  }
										  ?>
										<span class="checkbox-custom checkbox-info">
										  <?php
										  
										  $k = 0;
										  foreach($opt as $opt):
										  if($opt !== ''){
											$k++;
											
										  ?>
											<input type="checkbox" id="ck<?php echo $getqu->IDQuest.'q'.$k?>" class="chkgen" name="anschk<?=$getqu->IDQuest?>[]" value="<?php echo $opt;?>"  <?php echo in_array($opt, $valarr)?"checked":"" ?> data-validetta="minChecked[1]">
											<label for="ck<?php echo $getqu->IDQuest.'q'.$k?>"><?php echo $opt;?></label>
										<?php }endforeach;?>
										</span>
										<!--<label for=-->
										<?php }elseif($getqu->t_a == "file"){?>
											<!--<input  class="form-control" type="file" name="file<?php echo $getqu->IDQuest?>"> -->
											<label class="field prepend-icon append-button file">
											  <span class="button btn-primary">Choose File</span>
											  <input type="file" class="gui-file" name="file<?php echo $getqu->IDQuest?>[]" id="file<?php echo $getqu->IDQuest?>" onchange="document.getElementById('uploader<?php echo $getqu->IDQuest?>').value = this.value;" multiple="multiple">
											  
											  <input type="text" class="gui-input" id="uploader<?php echo $getqu->IDQuest?>" placeholder="Please Select A File">
											<font color='red'> Please upload only pdf, document or image format (.PDF,.DOC,.DOCX,.JPG,.PNG,.JPEG) </font>
											  <label class="field-icon">
												<i class="fa fa-upload"></i>
											  </label>
											</label>
											<div class="divfiles">
											<?php if(isset($filledans[$i-1]['fname']) && $filledans[$i-1]['fname'] !== ''){
											 $files = $this->case_model->getfilesbyref($filledans[$i-1]['anstxt']);
											 foreach($files as $file):
											 if (preg_match('/image/',$file->f_type)) {
											 ?>
											 <label>
												<a href="<?php echo site_url('uploads/'.$file->f_name)?>" data-lightbox="image-<?php echo $i-1?>" class="smthumbnail" ><img src="<?php echo site_url('uploads/'.$file->f_name)?>"/></a>
											</label>
											 
										<?php }elseif(preg_match('/pdf/',$file->f_type)){
										  echo '<label style="margin-top:15px;"><a href="'.base_url().'uploads/'.$file->f_name.'" target="_blank"><i class="fa fa-file-pdf-o fa-5x"></i></a></label>';
										}elseif(preg_match('/document/',$file->f_type)){
										  echo '<label style="margin-top:15px;"><a href="'.base_url().'uploads/'.$file->f_name.'" target="_blank"><i class="fa fa-file-text-o fa-5x"></i></a></label>';
										}
										  
										  ?>
										 
										<?php endforeach; }?>
										</div>
										<?php }?>
										
									<!--data-validetta="required" data-vd-message-required="Please select a file to upload"-->
									</div>
									<div><br></div>
									<!--<div class="showquesnote">-->
									<!--   <?php //if(isset( $getqu->note)){?>-->
									<!--  <label class="col-lg-4 control-label" for="inputStandard"> Note:</label>-->
									<!--  <div class="col-lg-8 notelabel" for="inputStandard"> :</div>-->
									<!--  <?php //}?>-->
									<!--</div>-->
									<div class="addeduserresponse">
									   <?php if(isset($filledans[$i-1]['userquery'])){?>
									   <label class="col-lg-4 control-label" for="inputStandard"> Query:</label>
									   <?php }else{?>
										  <label class="col-lg-4 control-label labqu hidden" for="inputStandard"> Query:</label>
									   <?php }?>
									   <div class="col-lg-8" style="padding-top:10px;">
									   <span class="labquery" style="margin-top:10px !important;"><?php echo isset($filledans[$i-1]['userquery'])?$filledans[$i-1]['userquery']:"";?></span>
									   <textarea class="gui-textarea textquery hidden" name="ansquery<?php echo $getqu->IDQuest?>[]" id="form-field-8"><?php //echo isset($get_all_question[$r]->note)?$get_all_question[$r]->note:''; ?></textarea>
									   </div>
									</div>
									
									
									</div>
								</div>
							</div>
							<div class="col-md-6 quesbt">
							<button class="btn btn-primary btn-sm addresques"  data-toggle="modal" data-target="#modaladdquery" data-ques="<?php echo $getqu->IDQuest?>" data-caseid="<?php echo $caseid?>"> <i class="fa fa-plus"></i> Add Response Query </button>
							
							</div>
						   <div class="col-md-12"> <hr/> </div>
								

							<?php  endforeach;?>

							
							<div class="col-md-12"> <hr/> </div>
							
							
							<div class="col-md-6">
								<div class="form-group">
								<label class="col-lg-4 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-8">
								 <button class="btn active btn-info" type="button" id="saveresponse" name="saveresponse">
								<i class="fa fa-save"></i>  Save </button>
								 
								<button class="btn active btn-success" type="submit" name="submitresponse">
								<i class="fa fa-save"></i>  Submit </button>
								
								<a href="javascript:window.history.back()" class="btn active btn-warning">
								<i class="fa fa-warning"></i>
								Cancel
								</a>
								
								
								</div>	
								</div>
							</div><div class="col-md-6"></div>
						</div><!-- end col-md-12 -->	
					</div>
				
			</form>
        </div>
	</div>
            <!-- End: Content -->  
</section>

<div class="modal fade" id="modaladdquery" tabindex="-1" role="dialog" aria-labelledby="modalAddQueryLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="modalAddQueryLabel">Add Query</h4>
            </div>
            <div class="modal-body">
                <form>
                    <textarea name="editor1" id="editor1" rows="10" cols="40"></textarea>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btaddquery" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url()."assets/js/lightbox.min.js"?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>media/vendor/validate/validetta.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>media/vendor/editors/ckeditor/ckeditor.js"></script>



<!--<script>-->
<!-- 	$.validator.setDefaults({-->
<!--		submitHandler: function(form) {-->
<!--			form.submit();-->
<!--		}-->
<!--	});-->
<!--       -->
<!--</script>-->

   <script type="text/javascript">
    $(document).ready(function() {
    $('#role_name').multiselect();
	
	$("#frmaddrespon").validetta({
	   realTime : true,
	    display : 'inline',
        errorTemplateClass : 'validetta-inline'
	  });


	  CKEDITOR.replace('editor1');
	  editor1 = CKEDITOR.instances.editor1;
	  $.fn.modal.Constructor.prototype.enforceFocus = function () {
		  modal_this = this
		  $(document).on('focusin.modal', function (e) {
			  if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
			  // add whatever conditions you need here:
			  &&
			  !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
				  modal_this.$element.focus()
			  }
		  })
	  };	  	

	  $('.addresques').on('click',function(e){
       e.preventDefault();
       $this =  $(this);
	   textnote = $(this).closest('.col-md-6').prev().find('.textquery');
	   labnote = $(this).closest('.col-md-6').prev().find('.labquery');
	   labqu = $(this).closest('.col-md-6').prev().find('.labqu');
	   console.log(textnote.val());
	   editor1.setData(textnote.val());
     });
	$('#btaddquery').click(function(){
	  var data = editor1.getData();
 	  if (data !== '') {
		 labqu.removeClass('hidden');
	  }
   	  console.log(data);
	  labnote.html(data);
	  textnote.val(data);
    });
	  
	});
	
	
    </script>
   <script src="<?php echo base_url()?>media/vendor/jquery/jquery.form.js"></script> 
   <script>
	  $(function(){
		 
		
		 $('#saveresponse').click(function(e){
			e.preventDefault();
			console.log('clicked');
			form = $(this).closest('form');
			url = "<?php echo base_url().'user_dashboard/saveresponse/'.$caseid?>";
			console.log(form.attr('id'));
			console.log(url);
			
			 options = { 
			   url: url,
			   success: function(data, statusText, xhr) {
			   console.log(data);
			   if (data == 1) {
				  alert('Your answer is saved');
				  window.history.back();
			   }
			   },
			   error: function(xhr, statusText, err) {
				   console.log(err || statusText);
			   }
			}; 
			 form.ajaxForm(options);
			 form.submit();
		 });
	  });
   </script>
 <script>
   $(function(){
	  $('.adddraftedanswer').click(function(e){
		 e.preventDefault();
		 $this = $(this);
		 caseid = $this.data('caseid');
		 quesid = $this.data('ques');
		 $.post("<?php echo base_url()."user_dashboard/getdraftanswer/"?>"+caseid+'/'+quesid,function(data){
			console.log(data);
			obj = $.parseJSON(data);
			console.log(obj);
			
			if (obj[0].t_a == "Text") {
			   $this.next('input[type="text"]').val(obj[0].saveanswer);
			}
		 });
		 
	  });
   });
 </script>
 
        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>       
        
        
