<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
   <script type="text/javascript" src="<?php echo base_url(); ?>media/assets/js/jquery-1.10.2.js"></script> 

</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">

       <?php echo $common_header;?>

       <?php echo $right_panel; ?>
     
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#"> View Report</a>
				</li>
			</ol>
		</div>
		
	   
	</header>
	
	<!-- End: Topbar -->

    <!-- Begin: Content -->
   
    <?php /*error_reporting(0);*/?>
     <div id="content" class="animated fadeIn">
        <div class="row">
      
			
            <form class="form-horizontal" id="frmaddmodule" method="post" action="">

				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span>View Report
							</span>
                        </div>
                        <div class="panel-body pn">
                         
                         <div class="admin-form">
                            
                            <div class="form-group">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-10"><?php //print_r($reportdata);?></div>
                                
                            
                            </div>
                            <?php foreach($reportdata as $reportdata){?>
                            <div class="form-group">
                                <span class="col-lg-2 labrep"><strong>Report Name:</strong></span>
                                <div class="col-lg-8"><strong><?php echo $reportdata->ReportName?></strong></div>
                                <div class="col-md-2"> &nbsp; </div>
                            </div>
                            
                            <div class="form-group">
                                <span class="col-lg-2 labrep"><strong>Case Description</strong></span>
                                <div class="col-lg-8"><strong><?php echo $reportdata->RepoDesc?></strong></div>
                                <div class="col-md-2"> &nbsp; </div>
                            </div>
                            
                            <div class="form-group">
                                <span class="col-lg-2 labrep"><strong>Module Name</strong></span>
                                <div class="col-lg-8"><strong><?php echo $modulename?></strong></div>
                                <div class="col-md-2"> &nbsp; </div>
                            </div>
						   <div class="form-group">
                                <span class="col-lg-2 labrep"></span>
                                <div class="col-lg-8"><strong><?php echo $reportdata->fcolhead?></strong></div>
                                <div class="col-md-2"> &nbsp; </div>
                            </div>

                            <?php if($reportdata->NoOfColumn == 1){?>
                            <div class="form-group">
                                <span class="col-lg-2 labrep">&nbsp;</span>
                                <div class="col-lg-8"><?php echo $reportdata->firstcol?></div>
                                <div class="col-md-2"> &nbsp; </div>
                            </div>
                            <?php }elseif($reportdata->NoOfColumn == 2){?>
                            <div class="form-group">
                                <div>
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <th><?php echo $reportdata->fcolhead?></th>
                                            <th><?php echo $reportdata->secolhead?></th>
                                        </thead>
                                        <tbody>
                                            <td><?php echo $reportdata->firstcol?></td>
                                            <td><?php echo $reportdata->secondcol?></td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php }elseif($reportdata->NoOfColumn == 3){?>
                              <div class="form-group">
                                <div>
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                            <th class='onethird'><?php echo $reportdata->fcolhead?></th>
                                            <th class='onethird'><?php echo $reportdata->secolhead?></th>
                                            <th class='onethird'><?php echo $reportdata->thirdcolhead?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td><?php echo $reportdata->firstcol?></td>
                                            <td><?php echo $reportdata->secondcol?></td>
                                            <td><?php echo $reportdata->thirdcol?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                          
                            <?php }?>
                            <?php }?>
                            <div class="form-group">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-10"><?php //print_r($reportdata);?></div>
                                
                            
                            </div> 


							<div class="col-md-12"> &nbsp; </div>
							<div class="col-md-12"> <hr/> </div>
                         </div>
						</div><!-- end col-md-12 -->	
					</div>
				</div>
			</form>
        </div>
	</div>
            <!-- End: Content -->  
</section>

        <!-- End: Content-Wrapper -->

<script>
function goBack() {
    window.history.back();
}
</script>   


        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>