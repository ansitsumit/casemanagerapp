<!-- Modal -->
<div id="modcaseseqlist" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Case Sequence</h4>
      </div>
      <div class="modal-body">
		<form>
		<ul id="tblcasesequence" class="sortable"></ul>
		<button type="button" class="btseqsort">GO</button>
		<span class="msgcaseseq"></span>
		</form>
		
	  </div>
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!---->