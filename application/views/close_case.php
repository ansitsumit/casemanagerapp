<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">
	
    <script src="<?php echo base_url();?>media/assets/js/jquery-1.10.2.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">

       <?php echo $common_header;?>

       <?php echo $right_panel;?>
     
   <?php $this->load->model('case_model');?>
    
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="javascript:location.reload()">Case Page</a>
				</li>
			</ol>
		</div>
		<!--<div class="topbar-right">    
			<a href="<?php echo base_url(); ?>cases/addcase"><button  class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button"><i class="fa fa-plus"></i>
            Add New</button></a>
		</div>-->
	   
	</header>
    <?php if($this->session->flashdata('msg')){ ?> 
    <!--########################## Message for update add edit delete ##################### ----------->
		<div class="col-md-12"> 
			<div class="alert alert-success dark alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa fa-check pr10"></i>
				 <?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
    <?php } ?>           
	<!--############### Main Body of Form start start , good alignment view notepad++  editer ####### ----------->

    <!-- Begin: Content --> 
    <div id="content">
        <div class="row">
			<div class="col-md-12">
			
				<div class="panel-body form-horizontal">
	
					<div class="form-group">
						<label class="col-lg-2 control-label" style="text-align:left;"> Party Names </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> <?php echo $caseItem[0]->FirstParty ?>&nbsp; | &nbsp;<?php echo $caseItem[0]->SecondParty ?></b>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label" style="text-align:left;"> Case Commenced </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> <?php echo $caseItem[0]->DateCommenced?> </b>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label" style="text-align:left;"> Amount in Controversy </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> <?php echo $caseItem[0]->AmountInDispute?> </b>
						</label>
					</div>
					
<!--					<div class="form-group">
						<label class="col-lg-2 control-label"> Target Mediation Date </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> 2015-06-22 </b>
						</label>
					</div>-->
					
				</div>
			</div>
			<?php //print_r($casesequenceorder);?>
				<div class="col-md-12">
					<h3> View Report  </h3>
					<div class="panel panel-body form-horizontal">
						<table id="reprttable" class="table table-bordered">
								<thead>
									<tr>
										<th> Steps </th>
										<th> Actions </th>
										<th> Deadlines </th>
										<!--<th>Status</th>-->
										<th>Report</th>
										
									</tr>	
								</thead>
								
								<tbody>
								 <?php
								 $i=1;
								 foreach($casemodule as $casemodule):
								 
								 ?>
									<tr>
									<?php
									$row =  $this->case_model->stepstatus($caseItem[0]->CaseID,$casemodule['Case_Sequence_ID'], $casemodule['side_authority']);
									$caseseqstatus =  $this->case_model->chkcaseseqstatus($caseItem[0]->CaseID,$casemodule['Case_Sequence_ID']);
									$caseseqstatact = isset($caseseqstatus->status)?$caseseqstatus->status:false;
									if(isset($row->status)){
									   switch($row->status){
										  case 1:
											 $acttxt = 'Not Completed';
											 break;
										  case 2:
											 $acttxt = 'In-Progress';
											 break;
										  case 3:
											 $acttxt = 'Completed';
									   }
									}
									?>
										<td class='colorrow<?php echo isset($row->status)?$row->status:1?>'>
										<?php if($caseseqstatact == 1){?>
										<a href="<?php echo base_url().'user_dashboard/responseuser/'.$caseItem[0]->CaseID."/".$casemodule['Case_Sequence_ID']?>"><?php echo "Step ".$i." (".$casemodule['input_module']?>)</a>
										<?php }else{
										  echo "Step ".$i." (".$casemodule['input_module'].")";
										  }?>
										</td>
										<td> <?php echo isset($row->status)?$acttxt:"Not Completed"?> </td>
										<td> 2015-12-22 </td>
										<!--<td><?php //echo isset($row->status)?$acttxt:"Not Completed"?></td>-->
										<td>
										<?php $result = $this->case_model->reportdata($casemodule['Case_Sequence_ID']);
											 $repcount = count($result);
											 if($repcount > 0){?>
											<a href="javascript:void(0)" data-sequenceid="<?php echo $casemodule['Case_Sequence_ID']?>" data-count="<?php echo $repcount?>" data-caseid="<?php echo $caseItem[0]->CaseID?>" data-utype="3" class="btn btn-danger btn-xs viewreport"><i class="fa fa-eye"></i> View Report</a>
											 <?php }else{
												echo "Not Available";
											 }?>  
										  </td>
										
									</tr>
									<?php $i++;endforeach;?>
									
								</tbody>
							
					   
						</table>
					</div>
				</div>
			      
        </div>
	</div>
            <!-- End: Content -->  
</section>
<!--Modal start-->
<div id="reportnav" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Party</h4>
      </div>
      <div class="modal-body">
		 <form>
		 <div><strong>Please select Which party you want to see </strong>&nbsp;
		 <select id="selectviewer">
			<option value="" selected>Select Report</option>
		 </select>
		 <input type="hidden" id="hid_mainurl"/>
		 </div>
		 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--Modal ends-->
        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>       
        
        
