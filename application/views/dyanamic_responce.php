<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">
	
    <!-- Casestyle CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
   <script type="text/javascript" src="<?php echo base_url(); ?>media/assets/js/jquery-1.10.2.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
    $('#role_name').multiselect();
    });
    </script>

</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">
       <?php echo $common_header;?>

       <?php echo $right_panel; ?>
     
       
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">Response Module</a>
				</li>
			</ol>
		</div>
		
	   
	</header>
	
	<!-- End: Topbar -->

    <!-- Begin: Content -->
   
     <div id="content" class="animated fadeIn">
        <div class="row">
      
			

<!---========== Add test ==========================------------------------------------------->
				<div class="col-md-12">
                    <div class="panel" id="spy4">
                        <div class="panel-headingcolor">
                            <span class="panel-title">
                                <span class="glyphicons glyphicons-table"></span> Response Module
							</span>
                        </div>
                        <div class="panel-body pn">
							<div class="col-md-12"> &nbsp; </div>
							<div>
								<div class="admin-form">
									<div class="form-group">
									   
									<label class="col-lg-2 control-label" for="inputStandard"> Main case Sequence :</label>
									<div class="col-lg-4">
										<label class="field select">
									<select id="main_case_sequence" name="main_case_sequence" required class="form-control">
										   <option value="" selected disabled>-- Select --</option>
										   <?php if(isset($case_sequence_list[0]->CaseSequenceID))
											for($i=0;$i<count($case_sequence_list);$i++) { ?>
											//$case_sequence_list[$i]->CaseSequenceID == $case_sequence_all[0]->Main_case_sequence_ID
											<?php echo "<option value='".$case_sequence_list[$i]->CaseSequenceID."'>".$case_sequence_list[$i]->SequenceName."</option>";
											 } ?>    
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									   <div class="col-lg-12"><br></div>
									</div>
									<div class="form-group">
									<label class="col-lg-2 control-label" for="inputStandard"> Input  Module :</label>
									<div class="col-lg-4">
										<label class="field select">
										<!--<select name="input_module" required class="form-control model" id="model" onchange="showUser(this.value)">-->
										<select id="inputmod" name="input_module" required class="form-control model">
										<option value="" selected disabled>-- Select --</option>
											   
										</select>
										<i class="arrow double"></i>
									</label>
									</div>
									<div class="col-lg-12"><br></div>
									</div>

								</div>	
								<div class="col-lg-12"><br></div>
							</div>

							
						</div><!-- end col-md-12 -->	
						 <div>
						   <div class="col-md-12 buttonpanel" style="background-color: #fff; padding-bottom: 10px; margin-top: -1px;">
								<div class="form-group">
								<label class="col-lg-2 control-label" for="inputStandard"> &nbsp; </label>
								<div class="col-lg-10">
								<button class="btn active btn-success" type="button" data-toggle="modal" data-target="#addquesmod"> 
								<i class="fa fa-plus"></i>  Add More </button>
								<button class="btn active btn-warning " type="button" onclick="javascript:window.history.back()">
					
									<i class="fa fa-warning"></i> Cancel </button>
								</div>	
								</div>
							</div>
						   <!--<div class="col-md-6" style="background-color: #ffffff;">&nbsp;</div>-->
						 </div>

					</div>
				</div>
        </div>
	</div>
            <!-- End: Content -->  
</section>


<div id="addquesmod" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Question</h4>
      </div>
      <div class="modal-body">
		<div>
			<div class="admin-form">
			   <form action="<?php echo base_url().'case_sequence/addques/' ?>" method="post" class="addquesfrm">
				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Question : </label>
				<div class="col-lg-8">
				<textarea class="gui-textarea" name="question" id="form-field-8"></textarea>
				<span class="errtxt red"></span>
				</div>
				</div>
				<p>&nbsp;</p>
				<?php echo form_hidden('moduleid')?>
				<div class="form-group">
				<label class="col-lg-4 control-label" for="inputStandard"> Response type:</label>
				<div class="col-lg-8">
				<label class="field select">
					<select class="t_a" name="t_a" id="selta">
						<option value="" selected>Select any</option>
						<option value="Radio">Radio Group</option>
						<option value="Checkbox" >Check Box</option>
						<option value="Text" >Text Box</option>
						<option value="file">Upload</option>
					</select>
					<i class="arrow double"></i>
				</label>
				<span class="errsel red"></span>
				
				</div>
				<div class="option_set"></div>
				</div>
				<div>
				  <p>&nbsp;</p>
				 <span class="centeralbutton"> 
				  <button class="btn active btn-success" type="submit"> 
								<i class="fa fa-plus"></i>  Add </button>
				 <button class="btn active btn-warning " type="button" data-dismiss="modal">Cancel</button>
				</span>
				</div>
			   </form>				
			</div>
		</div>
	  
	  </div>
      <div class="modal-footer">
        <span class="errmsg red pull-left"></span>
      </div>
    </div>

  </div>
</div>

<script>
   $(document).ready(function(){
	  $('.buttonpanel').hide();
	  $('#form-field-8').keypress(function(){
		 $('.errtxt').html('');
	  });
	  
	  $('#form-field-8').on('blur',function(){
	  if ($(this).val() == '') {
		 $('.errtxt').html('Please fill the question');
		}
	  });
	  
	  $('#selta').on('change',function(){
		 $('#selta option:first' ).hide();
		 $('.errsel').html('');
	  });
   
   })
</script>

        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>       
        
        
