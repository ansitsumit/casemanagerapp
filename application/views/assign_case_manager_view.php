<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Case management</title>
    <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
    <meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="AdminDesigns">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/skin/default_skin/css/theme.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>media/assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>media/assets/img/favicon.ico">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/casestyle.css">
    
	
	<script src="<?php echo base_url();?>media/assets/js/jquery-1.10.2.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<body class="dashboard-page sb-l-o sb-r-c">
	 <!-- Start: Main -->
    <div id="main">

       <?php echo $common_header;?>

       <?php echo $right_panel;?>
     
       
    <!-- Start: Content-Wrapper -->
<section id="content_wrapper"
	<!-- Start: Topbar -->
	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					<a href="#">Case Page</a>
				</li>
			</ol>
		</div>
		<!--<div class="topbar-right">    
			<a href="<?php echo base_url(); ?>cases/addcase"><button  class="btn btn-success btn-sm light fw600 ml10 pull-right" type="button"><i class="fa fa-plus"></i>
            Add New</button></a>
		</div>-->
	   
	</header>
    <?php if($this->session->flashdata('msg')){ ?> 
    <!--########################## Message for update add edit delete ##################### ----------->
		<div class="col-md-12"> 
			<div class="alert alert-success dark alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<i class="fa fa-check pr10"></i>
				 <?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>
    <?php } ?>           
	<!--############### Main Body of Form start start , good alignment view notepad++  editer ####### ----------->

    <!-- Begin: Content --> 
    <div id="content">
        <div class="row">
			<div class="col-md-12">
			
				<div class="panel-body form-horizontal">
		 			<div class="form-group">
						<label class="col-lg-2 control-label"> Party Names </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> <?php echo $caseItem[0]->FirstParty ?>&nbsp; | &nbsp;<?php echo $caseItem[0]->SecondParty ?></b>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label"> Amount in Controversy </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> $<?php echo $caseItem[0]->AmountInDispute ?> </b>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label"> Case Commenced </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> <?php echo $caseItem[0]->DateCommenced?> </b>
						</label>
					</div>
					
					<div class="form-group">
						<label class="col-lg-2 control-label"> Target Mediation Date </label>
						<label class="col-lg-10 control-label" style="text-align:left;">
							<b> </b>
						</label>
					</div>
					
				</div>
			</div>
			
			
			<form class="form-horizontal" role="form">
			   <?php
			   $this->load->model('case_model');
			   //print_r($casemodule);
			   
			   ?>
				<div class="col-md-12">
					<h3> View Report </h3>
					<div class="panel panel-body form-horizontal">
						<table class="table  table-bordered">
								<thead>
									<tr>
										<th> Case #<?php echo $caseItem[0]->CaseID;?></th>
										<th>Party</th>
										<th> Manager Actions</th>
										<th> Manager Comments</th>
										<th>status</th>
										<th>Follow Up</th>
										<th>Report</th>
									</tr>	
								</thead>
								<tbody>
								 <?php foreach($casemodule as $casemodule):?>
								 <tr>
									<?php
									$row =  $this->case_model->stepstatus($caseItem[0]->CaseID,$casemodule->Case_Sequence_ID, $casemodule->side_authority);
									if(isset($row->is_reviewed)){
									   if($row->is_reviewed == 0){
										  $revstat = 'Not Reviewed';
									   }else{
										  $revstat = 'Reviewed';
									   }
									}else{
									   $revstat = 'Not Reviewed';
									}
									?>
									<td class='colorrow<?php echo isset($row->status)?$row->status:1?>'>
									   
									   <a href="javascript:void(0)" data-link="<?php echo base_url().'manager_dashboard/viewcasedetails/'.$caseItem[0]->CaseID.'/'.$casemodule->Case_Sequence_ID.'/'.$casemodule->side_authority;?>" class="viewcasedetlink"><?php echo $casemodule->input_module?></a>
									   </td>
									<td><?php echo ($casemodule->side_authority == '1@2')?"Both":$casemodule->side_authority?></td>
									<td><?php echo $revstat ;?></td>
									<td>
									<?php $viewcom = $this->case_model->viewmanagercomm($caseItem[0]->CaseID,$casemodule->Case_Sequence_ID,$casemodule->side_authority);
									   foreach($viewcom as $viewcom){
										  echo $viewcom->comment."<br>";
									   }
									?></td>
									<td><a class="btn btn-success btn-xs purple chgstatbut" href="#" data-id="<?php echo $caseItem[0]->CaseID?>" data-toggle="modal" data-userno="<?php echo  $casemodule->side_authority?>" data-status="<?php echo isset($row->status)?$row->status:1?>" data-step="<?php echo $casemodule->Case_Sequence_ID?>" data-target="#chgsta"><i class="fa fa-magic"></i> status</a></td>
									<td>
									     
									   <a class="btn btn-info btn-xs purple viewcasedetlink" href="javascript:void(0)" data-link="<?php echo base_url().'manager_dashboard/followupstep/'.$caseItem[0]->CaseID.'/'.$casemodule->Case_Sequence_ID.'/'.$casemodule->side_authority ?>" data-id="<?php echo $caseItem[0]->CaseID?>" data-userno="<?php echo $casemodule->side_authority?>" data-step="<?php echo $casemodule->Case_Sequence_ID?>" ><i class="fa fa-gears"></i> Follow Up Step</a></td>
									<td>
									   <?php $result = $this->case_model->reportdata($casemodule->Case_Sequence_ID);
											 $repcount = count($result);
											 if($repcount > 0){?>
											<a href="javascript:void(0)" data-sequenceid="<?php echo $casemodule->Case_Sequence_ID?>" data-count="<?php echo $repcount?>" data-caseid="<?php echo $caseItem[0]->CaseID?>" data-utype="2" class="btn btn-danger btn-xs viewreport"><i class="fa fa-eye"></i> View Report</a>
											 <?php }else{
												echo "Not Available";
											 }?>  
									</td>
								 </tr>
								 <?php endforeach;?>								
								</tbody>
						</table>
					</div>
				</div>
			</form>      
        </div>
	</div>
            <!-- End: Content -->  
</section>
<!--Modal start-->
<div id="confirmmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Party</h4>
      </div>
      <div class="modal-body">
		 <form>
		 <div><strong>Please select Which party you want to see </strong>&nbsp;
		
		 <select id="selectparty">
			<option value="0" selected="selected">Select Party</option>
			<option value="1">Party 1</option>
			<option value="2">Party 2</option>
		 </select>
		 <input type="hidden" id="hid_mainurl"/>
		 </div>
		 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--Modal ends-->

<!--Modal start-->
<div id="reportnav" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Party</h4>
      </div>
      <div class="modal-body">
		 <form>
		 <div><strong>Please select Which party you want to see </strong>&nbsp;
		 <select id="selectviewer">
			<option value="" selected>Select Report</option>
		 </select>
		 <input type="hidden" id="hid_mainurl"/>
		 </div>
		 </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--Modal ends-->
        <!-- End: Content-Wrapper -->
        
 <?php echo $common_footer ?>       
        
        
