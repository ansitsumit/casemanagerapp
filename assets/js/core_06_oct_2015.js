/*core.js */
//console.log(document.location.hostname);
 if (document.location.hostname == "localhost" || document.location.hostname == "ansitsdev.com"){
        baseurl = window.location.origin+"/casemanagementapp/";
   }else{
        baseurl = window.location.origin+"/casemanagementapp/";
   }




   function checknewnotification(role) {
    url = baseurl+"notification/shwnotification/"+role;
    firstid = $('.noticecom').first().data('id');
      $.ajax({
        type: "POST",
        url:url,
        success: function(output){
            obj = $.parseJSON(output);
            //obj = obj.reverse();
            //var last = (last=Object.keys(obj))[last.length-1];
            //lastid = obj[last].id;
            var data = $.grep(obj, function(e){
                return e.id >  firstid;
            });
            data = data.reverse();
            $.each(data, function(i, item) {
                //console.log(obj[i].comment);
                $('#noticediv').prepend('<p class="pnotice"><a href="javascript:void(0)" class="noticecom" data-toggle="modal" data-target="#notimodal" data-id='+data[i].id+'>'+data[i].comment+' on '+data[i].created+'</a></p>');
                return (obj[i].id != firstid);
            });

        }
      })
   }  


function retrivechat(managerid,userid,lastid,pagetype){
       //console.log("managerid "+managerid);
       //console.log("userid "+userid);
       //console.log("lastid "+lastid);
       $.ajax({
              type:"POST",
              url:baseurl+'user/getnewchat/',
              data:{managerid:managerid,userid:userid,lastid:lastid},
              success:function(output){
                      data = $.parseJSON(output);
                     //console.log(output);
                     $.each(data, function(i,value){
                        if(pagetype == 2 && data[i].type == 2){
                            liclass  = "out";
                        }else if (pagetype == 1 && data[i].type == 1) {
                            liclass  = "out";
                        }else{
                            liclass  = "in";
                            }
                         var img = '<img class="avatar img-responsive" alt=""src="'+baseurl+'/media/profile_img/manager/audi.jpg">';

                         $('.chats').append('<li class='+liclass+' data-msgid="'+data[i].msgid+'">'+img+'<div class="message"><span class="arrow"></span><span class="name">'+data[i].response+'</span><span class="datetime"> '+data[i].date+' '+data[i].time+'</span><span class="body">'+data[i].msg+'</span></div></li>');
                     });
                        if (output !== '[]') {
                         $('.megachat').animate({
                            scrollTop: $('.chats li:last-child').offset().top + '15px'
                            }, 1000);
                     }
                  
              }
              
       });
}



$(document).ready(function(){

       $('.sliderupl').on('click',function(){
              $(this).next().find('.showupl').SlideToggle();
       });
       
      $('body').on('click','.noticecom',function(){
             var id = $(this).data('id');
             $.ajax({
              type:"POST",
              url:baseurl+"notification/noticedetails/"+id,
              success: function(output){
                     console.log(output);
                     obj = $.parseJSON(output);
                     $('#notid').html(obj.id);
                     $('.mbpara').html(obj.comment);
                     $('#notcreattime').html(obj.created);
                     $('#notcreater').html(obj.name);
                     $('#role').html(obj.role);
                     
              }
              });
      });
      
      $('#frmaddcom').on('submit',function(e){
              e.preventDefault();
              var txtcom = $('input[name="txtcom"]').val();
              var caseid = $('input[name="caseid"]').val();
              var partyno = $('input[name="partyno"]').val();
              if (txtcom == '') {
                     return false;
              }
              var $form = $(this);
              var pc = "<p>"+txtcom+"</p>"
              var formdata = $form.serialize();
             url = $form.attr('action');
              $.ajax({
                 type:"POST",
                 url:url,
                 data:formdata,
                 success:function(output){
                     //console.log(output);
                     if (output !== 0) {
                          $('.commentsection').append(output);
                          $('input[name="txtcom"]').val('');
                          $.post(baseurl+'manager_dashboard/executnotify/',{caseid:caseid,txtcom:txtcom,partyno:partyno},function(data){});
                     }
                 }
              });
      });
      
     $('[data-toggle="tooltip"]').tooltip();
     $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    
    
    $('body').on('click','.delcom',function(){
       var pc =  $(this).closest('p');
       var id = pc.attr('id');
       url = baseurl+"manager_dashboard/delmanacom/"+id;
       $.ajax({
          type:"POST",
          url: url,
          success:function(output){
              if (output == 1) {
                     pc.remove();
              }
          }
          
       });
    });

   $('.chgstatbut').on('click',function(){
       $('input[name="stepno"]').val($(this).data('step'));
       $('input[name="partyno"]').val($(this).data('userno'));
       //formdata = {step:step,partyno:partyno,caseid:caseid,status:status};
   });
   
   $('#frmcasesteps').on('submit',function(e){
       e.preventDefault();
       var $form = $(this);
       var url = $form.attr('action');
       var formdata = $form.serialize();
       $.ajax({
          type:"POST",
          url:url,
          data:formdata,
          success:function(output){
              if (output == 1) {
                     $('.showmsg').removeClass('red');
                     $('.showmsg').addClass('blue');
                     $('.showmsg').html('Successfully changed the state');
                     $('.showmsg').fadeIn(900,'swing');
                     setTimeout(function(){
                            location.reload();
                     },4000);
                     
              }else if (output == 2){
                     $('.showmsg').removeClass('blue');
                     $('.showmsg').addClass('red');
                     $('.showmsg').html('You are changing to same state');                     
              }else{
                     $('.showmsg').removeClass('blue');
                     $('.showmsg').addClass('red');
                     $('.showmsg').html('Same state');
              }
              $('.showmsg').fadeIn(900,'swing');
          }
       });
   });
   
   $('#setchatmessage').on('submit',function(e){
      e.preventDefault();
      var chatdis  = $('#chatdis').val();
      if(chatdis.trim()== ''){return false;}else{
       var $form = $(this);
       var url = $form.attr('action');
       var txtchat = $('#chatdis').val();
       $.ajax({
              type:"POST",
              url:url,
              data:{"discussion":txtchat},
              success:function(output){
                     console.log(output);
                     if (output == 1) {
                         $('#chatdis').val('');
                     }
              }
        });
      }
   });
   
       $( ".new_question" ).click(function() {
              $.post( baseurl+"jquery_pop/Test", { id: "k" }, function( data ) {
                     $(".addnewquestion").append(data);
             
             });
       });
       
       $('body').on("click",".removequestion",function() {
              console.log("hello");
              $(this).parent().parent().parent().parent().parent().parent().remove();
              //$(this).closest('.col-md-12').remove();
       });
       

     
chk = '<div class="col-md-12 form-horizontal"> <br/><div class="admin-form"><div class="form-group"><label class="col-lg-4 control-label" for="inputStandard"> 1 .</label><div class="col-lg-8"><input  class="form-control" type="text" name="options[]"> </div></div><div class="form-group"><label class="col-lg-4 control-label" for="inputStandard"> 2 .</label><div class="col-lg-8"><input  class="form-control" type="text" name="options[]"> </div></div><div class="form-group"><label class="col-lg-4 control-label" for="inputStandard"> 3 .</label><div class="col-lg-8"><input  class="form-control" type="text" name="options[]"> </div></div><div class="form-group"><label class="col-lg-4 control-label" for="inputStandard"> 4 .</label><div class="col-lg-8"><input  class="form-control" type="text" name="options[]"> </div></div></div></div>';
     
     
     $(".t_a").each(function(){
       if($(this).val() == "Text"){
       $(this).parent().parent().parent().find(".option_set div").remove();
     }
     });
       $('body').on('change',".t_a",function(){
              $(this).parent().parent().parent().find(".option_set div").remove();
              if($(this).val()=="Radio"){
                     console.log("checkbox");
                     //$(this).parent().parent().parent().find(".option_set").load(baseurl+'jquery_pop/option_checkbox');
                     
                     $(this).parent().parent().parent().find(".option_set").append(chk);
              }else if ($(this).val()=="Checkbox") {
                     console.log("checkbox");
                     //$(this).parent().parent().parent().find(".option_set").load(baseurl+"jquery_pop/option_checkbox");
                     $(this).parent().parent().parent().find(".option_set").append(chk);
              }else if ($(this).val()=="Text") {
                     //$(this).parent().parent().parent().find(".option_set").html("<input type='hidden'name='options[]'><input type='hidden'name='options[]'><input type='hidden'name='options[]'><input type='hidden'name='options[]'>");
                     //$(this).parent().parent().parent().find(".option_set div").remove();
              }else if($(this).val()=="file"){
                     $(this).parent().parent().parent().find(".option_set").load(baseurl+'jquery_pop/option_fileupl');
              }
       });




       
});

