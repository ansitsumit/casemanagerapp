
//-------------for update userprofile validation ------------------

$(".user_update").click(function (){
	
	if($(".FirstName").val()==""){
		alertify.error(" First Name is empty !");
		$(".FirstName").focus();
		return false;
	}
	if($(".LastName").val()==""){
		alertify.error(" Last Name is empty !");
		$(".LastName").focus();
		return false;
	}
	if($(".Address").val()==""){
		alertify.error("Address is empty !");
		$(".Address").focus();
		return false;
	}
	
	if($(".City").val()==""){
		alertify.error("Cityis empty !");
		$(".City").focus();
		return false;
	}
	if($(".State").val()==""){
		alertify.error("State is empty !");
		$(".State").focus();
		return false;
	}
	
	if($(".Zip").val()==""){
		alertify.error("Zip is empty !");
		$(".Zip").focus();
		return false;
	}
	var zip = /^[0-9]{5}$/;
	if(!zip.test($(".Zip").val()))
	{	
		alertify.error("enter valid Five digit Zip!");
		$(".Zip").focus();
		return false;
	}
	
	if($(".Email").val()==""){
		alertify.error("Email  is empty !");
		$(".Email").focus();
		return false;
	}
	
	var ema = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!ema.test($(".Email").val()))
	{	
		alertify.error("Please enter valid email !");
		$(".Email").focus();
		return false;
	}
	
	
	if($(".Password").val()==""){
		alertify.error("Password is empty !");
		$(".Password").focus();
		return false;
	}
	
	if($(".PhoneNumber").val()==""){
		alertify.error("PhoneNumber is empty !");
		$(".PhoneNumber").focus();
		return false;
	}
	var phone = /^[0-9]{10}$/;
	if(!phone.test($(".PhoneNumber").val()))
	{	
		alertify.error("enter ten digit valid  Phone !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	if($(".position").val()==""){
		alertify.error("position is empty !");
		$(".position").focus();
		return false;
	}
	
	
	alertify.success("Data Successfully Validate");	
});

