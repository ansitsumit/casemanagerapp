///-----------for Add user validaton ----------------


$("#submit_btn").click(function (){
	if($(".FirstName").val()==""){
		alertify.error("First Name is empty !");
		$(".FirstName").focus();
		return false;
	}
	if($(".LastName").val()==""){
		alertify.error("Last Name is empty !");
		$(".LastName").focus();
		return false;
	}
	
	if($(".Position").val()==""){
		alertify.error("Position is empty !");
		$(".Position").focus();
		return false;
	}
	
	
	if($(".Address").val()==""){
		alertify.error("Address is empty !");
		$(".Address").focus();
		return false;
	}
	
	if($(".City").val()==""){
		alertify.error("City is empty !");
		$(".City").focus();
		return false;
	}
	if($(".State").val()==""){
		alertify.error("State is empty !");
		$(".State").focus();
		return false;
	}
	
	
	if($(".Zip").val()==""){
		alertify.error("Zip is empty !");
		$(".Zip").focus();
		return false;
	}
	
	var ph = /^[0-9]{5}$/;
	if(!ph.test($(".Zip").val()))
	{	
		alertify.error("enter five digit valid zip code !");
		$(".Zip").focus();
		return false;
	}
	if($(".Email").val()==""){
		alertify.error("Email  is empty !");
		$(".Email").focus();
		return false;
	}
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!re.test($(".Email").val()))
	{	
		alertify.error("Please enter valid email address !");
		$(".Email").focus();
		return false;
	}
	
	if($(".Password").val()==""){
		alertify.error("Password is empty !");
		$(".Password").focus();
		return false;
	}
	
if($(".Con_Password").val()==""){
		alertify.error("Con_Password empty !");
		$(".Con_Password").focus();
		return false;
	}
	
	if($(".Con_Password").val() != $(".Password").val())
	{
	alertify.error("Conform Password Not match !");
		$(".Con_Password").focus();
		return false;	
	}
	
	
	

	if($(".PhoneNumber").val()==""){
		alertify.error("Phone Number is empty !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	var pho = /^[0-9]{10}$/;
	if(!pho.test($(".PhoneNumber").val()))
	{	
		alertify.error("Enter Ten digit valid Phone Number !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	if($(".Firm_CompanySize").val()==""){
		alertify.error("Firm Company Size empty !");
		$(".Firm_CompanySize").focus();
		return false;
	}
	if($(".HowDidYouHearAboutUs").val()==""){
		alertify.error("How Did You Hear About Us empty !");
		$(".HowDidYouHearAboutUs").focus();
		return false;
	}
		
});

///-----------for update user validaton ----------------


$("#update").click(function (){
	if($(".FirstName").val()==""){
		alertify.error("First Name is empty !");
		$(".FirstName").focus();
		return false;
	}
	if($(".LastName").val()==""){
		alertify.error("Last Name is empty !");
		$(".LastName").focus();
		return false;
	}
	
	if($(".Position").val()==""){
		alertify.error("Position is empty !");
		$(".Position").focus();
		return false;
	}
	
	
	if($(".Address").val()==""){
		alertify.error("Address is empty !");
		$(".Address").focus();
		return false;
	}
	
	if($(".City").val()==""){
		alertify.error("City is empty !");
		$(".City").focus();
		return false;
	}
	if($(".State").val()==""){
		alertify.error("State is empty !");
		$(".State").focus();
		return false;
	}
	
	
	if($(".Zip").val()==""){
		alertify.error("Zip is empty !");
		$(".Zip").focus();
		return false;
	}
	
	var ph = /^[0-9]{5}$/;
	if(!ph.test($(".Zip").val()))
	{	
		alertify.error("enter five digit valid zip code!");
		$(".Zip").focus();
		return false;
	}
	if($(".Email").val()==""){
		alertify.error("Email  is empty !");
		$(".Email").focus();
		return false;
	}
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!re.test($(".Email").val()))
	{	
		alertify.error("Please enter valid email address !");
		$(".Email").focus();
		return false;
	}
	
	if($(".Password").val()==""){
		alertify.error("Password is empty !");
		$(".Password").focus();
		return false;
	}
	

	if($(".PhoneNumber").val()==""){
		alertify.error("Phone Number is empty !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	var ph = /^[0-9]{10}$/;
	if(!ph.test($(".PhoneNumber").val()))
	{	
		alertify.error("enter Ten digit valid Phone Number !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	if($(".Firm_CompanySize").val()==""){
		alertify.error("Firm Company Size empty !");
		$(".Firm_CompanySize").focus();
		return false;
	}
	if($(".HowDidYouHearAboutUs").val()==""){
		alertify.error("How Did You Hear About Us empty !");
		$(".HowDidYouHearAboutUs").focus();
		return false;
	}
		
});

$("#add_assign").click(function (){
	
	if($(".CaseID").val()==""){
		alertify.error("Case is empty !");
		$(".CaseID").focus();
		return false;
	}
	if($(".Case_Sequence_ID").val()==""){
		alertify.error("Case Sequence  is empty !");
		$(".Case_Sequence_ID").focus();
		return false;
	}
	
});