$(".submit_register_btn").click(function (){
	if($(".FirstName").val()==""){
		alertify.error("First Name is empty !");
		$(".FirstName").focus();
		return false;
	}
	if($(".LastName").val()==""){
		alertify.error("Last Name is empty !");
		$(".LastName").focus();
		return false;
	}
	if($(".Position").val()==""){
		alertify.error("Position is empty !");
		$(".Position").focus();
		return false;
	}
	if($(".Address").val()==""){
		alertify.error("Address is empty !");
		$(".Address").focus();
		return false;
	}
	if($(".City").val()==""){
		alertify.error("City is empty !");
		$(".City").focus();
		return false;
	}
	if($(".State").val()==""){
		alertify.error("State is empty !");
		$(".State").focus();
		return false;
	}
	if($(".Zip").val()==""){
		alertify.error("Zip is empty !");
		$(".Zip").focus();
		return false;
	}
	
	var zp = /^[0-9]{6}$/;
	if(!zp.test($(".Zip").val()))
	{	
		alertify.error("Please enter valid Zip !");
		$(".Zip").focus();
		return false;
	}
	
	if($(".Email").val()=="" )
	{
		alertify.error("Email is empty !");
		$(".Email").focus();
		return false;
	}
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!re.test($(".Email").val()))
	{	
		alertify.error("Please enter valid email address !");
		$(".Email").focus();
		return false;
	}
	

	if($(".Password").val()==""){
		alertify.error("Password is empty !");
		$(".Password").focus();
		return false;
	}
	if($(".Con_Password").val()==""){
		alertify.error("Conform Password is empty !");
		$(".Con_Password").focus();
		return false;
	}
	if($(".Con_Password").val() != $(".Password").val())
	{
	alertify.error("Conform Password Not match !");
		$(".Con_Password").focus();
		return false;	
	}
	if($(".PhoneNumber").val()==""){
		alertify.error("Phone Number is empty !");
		$(".PhoneNumber").focus();
		return false;
	}
	
	var length = 11;
	if(!length.test($(".PhoneNumber").val()))
	{	
		alertify.error("Please enter 11 digit Mobile number !");
		$(".PhoneNumber").focus();
		return false;
	}
	if($(".Firm_CompanySize").val()==""){
		alertify.error("Firm Company Size empty !");
		$(".Firm_CompanySize").focus();
		return false;
	}
	
	alertify.success("Data Successfully Submit");
	$('form#loginform').submit();
});


//--------------  Manager -------------------------


$(".btnmanager").click(function (){
	if($(".Name").val()==""){
		alertify.error("Name is empty !");
		$(".Name").focus();
		return false;
	}
	if($(".Address").val()==""){
		alertify.error("Address is empty !");
		$(".Address").focus();
		return false;
	}
	if($(".Phone").val()==""){
		alertify.error("Phone is empty !");
		$(".Phone").focus();
		return false;
	}
	
	var phone = /^[0-9]{10}$/;
	if(!phone.test($(".Phone").val()))
	{	
		alertify.error("Enter ten digit Phone number !");
		$(".Phone").focus();
		return false;
	}
	
	if($(".Email").val()=="" )
	{
		alertify.error("Email is empty !");
		$(".Email").focus();
		return false;
	}
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!re.test($(".Email").val()))
	{	
		alertify.error("Please enter valid email address !");
		$(".Email").focus();
		return false;
	}
	if($(".Biography").val()==""){
		alertify.error("Biography is empty !");
		$(".Biography").focus();
		return false;
	}
	if($(".Areasofexpertise").val()==""){
		alertify.error("Areas of expertiser is empty !");
		$(".Areasofexpertise").focus();
		return false;
	}

	if($(".Passsword").val()==""){
		alertify.error("Passsword is empty !");
		$(".Passsword").focus();
		return false;
	}
	
	
	//alertify.success("Data Successfully Submit");
	//$('form#loginform').submit();
});


///-----------for edit case user----------------


$("#submit_btn").click(function (){
	if($(".ManagerAssigned").val()==""){
		alertify.error("Manager Assigned is empty !");
		$(".ManagerAssigned").focus();
		return false;
	}
	if($(".date").val()==""){
		alertify.error("Date Commenced is empty !");
		$(".date").focus();
		return false;
	}
	
	//if($('input[name=firstparty_represnt]:checked').length<=0)
	//{
	//	alertify.error("Select you are representing this party ? ");
	//	//$(".CaseTitle").focus();
	//	return false;
	//}
	if(($('input[name=secondparty_represnt]:checked').length<=0) && ($('input[name=firstparty_represnt]:checked').length<=0)){
		alertify.error("Please represent first party or second party! ");
		return false;		
	}		
	
	if($('input[name=represnt]:checked').length<=0)
	{
		alertify.error("Select  yor are Plaintiff or Defendant  ? ");
		//$(".CaseTitle").focus();
		return false;
	}
	
	if($(".CaseTitle").val()==""){
		alertify.error("Case Title is empty !");
		$(".CaseTitle").focus();
		return false;
	}
	
	
	if($(".FirstParty").val()==""){
		alertify.error("First Party Name is empty !");
		$(".FirstParty").focus();
		return false;
	}
	
	if($(".AddressOfFirstParty").val()==""){
		alertify.error("Address Of First Party is empty !");
		$(".AddressOfFirstParty").focus();
		return false;
	}
	if($(".FirstPartyEmail").val()==""){
		alertify.error("First Party Email is empty !");
		$(".FirstPartyEmail").focus();
		return false;
	}
	
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!re.test($(".FirstPartyEmail").val()))
	{	
		alertify.error("Please enter valid email address First Party !");
		$(".FirstPartyEmail").focus();
		return false;
	}
	if($(".FirstPArtyPhone").val()==""){
		alertify.error("First Party Phone is empty !");
		$(".FirstPArtyPhone").focus();
		return false;
	}
	
	
	var ph = /^[0-9]{10}$/;
	if(!ph.test($(".FirstPArtyPhone").val()))
	{	
		alertify.error("enter valid First Party Phone!");
		$(".FirstPArtyPhone").focus();
		return false;
	}
	if($(".AttorneyNameFirst_party").val()==""){
		alertify.error("Attorney Name of First party  is empty !");
		$(".AttorneyNameFirst_party").focus();
		return false;
	}
	
	if($(".AmountInDisputeFirst_party").val()==""){
		alertify.error("Amount In Dispute First party  is empty !");
		$(".AmountInDisputeFirst_party").focus();
		return false;
	}
if($(".FirstParty_firm_name").val()==""){
		alertify.error("First Party firm name is empty !");
		$(".FirstParty_firm_name").focus();
		return false;
	}
	
	if($(".FirstParty_attorney_email").val()==""){
		alertify.error("First Party attorney email  is empty !");
		$(".FirstParty_attorney_email").focus();
		return false;
	}
	
	var ree = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!ree.test($(".FirstParty_attorney_email").val()))
	{	
		alertify.error("Please enter valid First Party  attorney email address!");
		$(".FirstParty_attorney_email").focus();
		return false;
	}
	
if($(".FirstPartyAttorney_address").val()==""){
		alertify.error("First Party Attorney address is empty !");
		$(".FirstPartyAttorney_address").focus();
		return false;
	}
	if($(".FirstPartyAttorney_phone").val()==""){
		alertify.error("First Party Attorney phone is empty !");
		$(".FirstPartyAttorney_phone").focus();
		return false;
	}
	
	var pho = /^[0-9]{10}$/;
	if(!pho.test($(".FirstPartyAttorney_phone").val()))
	{	
		alertify.error("enter valid First Party Attorney Phone!");
		$(".FirstPartyAttorney_phone").focus();
		return false;
	}
	
	if($(".FirstPartyCase_type").val()==""){
		alertify.error("First Party case type is empty !");
		$(".FirstPartyCase_type").focus();
		return false;
	}
	if($(".FirstPartyCase_desctiption").val()==""){
		alertify.error("First Party Case Description is empty !");
		$(".FirstPartyCase_desctiption").focus();
		return false;
	}
	
	
	//if($('input[name=secondparty_represnt]:checked').length<=0)
	//{
	//	alertify.error("Select you are representing this party ? ");
	//	$('input[name=secondparty_represnt]:checked').focus();
	//	return false;
	//}
	if($('input[name=secondpartyPlain_defendant]:checked').length<=0)
	{
		alertify.error("Select  yor are Plaintiff or Defendant  ? ");
		//$(".CaseTitle").focus();
		return false;
	}
	
	
	
	if($(".SecondParty").val()==""){
		alertify.error("Second Party Name is empty !");
		$(".SecondParty").focus();
		return false;
	}
	if($(".AddressOfSecondParty").val()==""){
		alertify.error("Address Of Second Party  is empty !");
		$(".AddressOfSecondParty").focus();
		return false;
	}
	if($(".SecondPartyEmail").val()==""){
		alertify.error("Second Party Email  is empty !");
		$(".SecondPartyEmail").focus();
		return false;
	}
	
	
	var reee = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!reee.test($(".SecondPartyEmail").val()))
	{	
		alertify.error("Enter valid email address of Second Party !");
		$(".SecondPartyEmail").focus();
		return false;
	}
	
	if($(".SecondPartyPhone").val()==""){
		alertify.error("Second Party Phone  is empty !");
		$(".SecondPartyPhone").focus();
		return false;
	}
	
	var phon = /^[0-9]{10}$/;
	if(!phon.test($(".SecondPartyPhone").val()))
	{	
		alertify.error("enter valid second Party  Phone!");
		$(".SecondPartyPhone").focus();
		return false;
	}
	
	if($(".SecondPartyAttorneyName").val()==""){
		alertify.error("Second Party Attorney Name  is empty !");
		$(".SecondPartyAttorneyName").focus();
		return false;
	}
	if($(".SecondPartyAmountInDispute").val()==""){
		alertify.error("Second arty Amount In Dispute is empty !");
		$(".SecondPartyAmountInDispute").focus();
		return false;
	}
	if($(".SecondParty_firm_name").val()==""){
		alertify.error("Second Party firm name is empty !");
		$(".SecondParty_firm_name").focus();
		return false;
	
	}
	if($(".SecondParty_attorney_email").val()==""){
		alertify.error("Second Party attorney email is empty !");
		$(".SecondParty_attorney_email").focus();
		return false;
	}
	
	
	var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(!email.test($(".SecondParty_attorney_email").val()))
	{	
		alertify.error("Enter valid Second Party attorney email !");
		$(".SecondParty_attorney_email").focus();
		return false;
	}
	if($(".AddressOfSecondParty_attorney").val()==""){
		alertify.error("Address Of Second Party attorney is empty !");
		$(".AddressOfSecondParty_attorney").focus();
		return false;
	}
	
	if($(".SecondParty_attorney_phone").val()==""){
		alertify.error("Second Party attorney phone is empty !");
		$(".SecondParty_attorney_phone").focus();
		return false;
	}
	
	var phone = /^[0-9]{10}$/;
	if(!phone.test($(".SecondParty_attorney_phone").val()))
	{	
		alertify.error("enter valid second Party Attorney Phone!");
		$(".SecondParty_attorney_phone").focus();
		return false;
	}
	
	if($(".SecondParty_case_type").val()==""){
		alertify.error("Second Party case type is empty !");
		$(".SecondParty_case_type").focus();
		return false;
	}
	
	if($(".case_description_SecondParty").val()==""){
		alertify.error("case description Second Partye is empty !");
		$(".case_description_SecondParty").focus();
		return false;
	}
	
	
	
	
});