<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Image Cropper Class
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
class imagic_resize {
	
	
	
function resize($imagePath,$opts=null)
{
	$imagePath = urldecode($imagePath);
	# start configuration
	$cacheFolder = 'cache/'; # path to your cache folder, must be writeable by web server
	$remoteFolder = 'remote/'; # path to the folder you wish to download remote images into

	$defaults = array('crop' => false, 'scale' => 'false', 'thumbnail' => false, 'maxOnly' => false, 
	   'canvas-color' => 'transparent', 'output-filename' => false, 
	   'cacheFolder' => $cacheFolder, 'remoteFolder' => $remoteFolder, 'quality' => 90, 'cache_http_minutes' => 20);

	$opts = array_merge($defaults, $opts);    

	$cacheFolder = $opts['cacheFolder'];
	$remoteFolder = $opts['remoteFolder'];

	$path_to_convert = 'convert'; # this could be something like /usr/bin/convert or /opt/local/share/bin/convert
	
	## you shouldn't need to configure anything else beyond this point

	$purl = parse_url($imagePath);
	$finfo = pathinfo($imagePath);
	$ext = $finfo['extension'];

	# check for remote image..
	if(isset($purl['scheme']) && ($purl['scheme'] == 'http' || $purl['scheme'] == 'https')):
		# grab the image, and cache it so we have something to work with..
		list($filename) = explode('?',$finfo['basename']);
		$local_filepath = $remoteFolder.$filename;
		$download_image = true;
		if(file_exists($local_filepath)):
			if(filemtime($local_filepath) < strtotime('+'.$opts['cache_http_minutes'].' minutes')):
				$download_image = false;
			endif;
		endif;
		if($download_image == true):
			$img = file_get_contents($imagePath);
			file_put_contents($local_filepath,$img);
		endif;
		$imagePath = $local_filepath;
	endif;

	if(file_exists($imagePath) == false):
		$imagePath = FILE_ROOT_PATH.$imagePath;
		if(file_exists($imagePath) == false):
			return 'image not found';
		endif;
	endif;

	if(isset($opts['w'])): $w = $opts['w']; endif;
	if(isset($opts['h'])): $h = $opts['h']; endif;

	$filename = md5_file($imagePath);

	// If the user has requested an explicit output-filename, do not use the cache directory.
	if(false !== $opts['output-filename']) :
		$newPath = $opts['output-filename'];
	else:
        if(!empty($w) and !empty($h)):
            $newPath = $cacheFolder.$filename.'_w'.$w.'_h'.$h.(isset($opts['crop']) && $opts['crop'] == true ? "_cp" : "").(isset($opts['scale']) && $opts['scale'] == true ? "_sc" : "").'.'.$ext;
        elseif(!empty($w)):
            $newPath = $cacheFolder.$filename.'_w'.$w.'.'.$ext;	
        elseif(!empty($h)):
            $newPath = $cacheFolder.$filename.'_h'.$h.'.'.$ext;
        else:
            return false;
        endif;
	endif;

	$create = true;

    if(file_exists($newPath) == true):
        $create = false;
        $origFileTime = date("YmdHis",filemtime($imagePath));
        $newFileTime = date("YmdHis",filemtime($newPath));
        if($newFileTime < $origFileTime): # Not using $opts['expire-time'] ??
            $create = true;
        endif;
    endif;
if($create == true)
	{
			
	$i = new Imagick(FILE_ROOT_PATH .'/'.$imagePath);
	
	if (array_key_exists('thumb', $opts))
	 {
		if(true === $opts['thumb'])
		{
			// get the current image dimensions
					$geo = $i->getImageGeometry();
					$width=$opts['w'];
					$height=$opts['h'];
				// crop the image
					if(($geo['width']/$width) < ($geo['height']/$height))
					{
						$i->cropImage($geo['width'], floor($height*$geo['width']/$width), 0, (($geo['height']-($height*$geo['width']/$width))/2));
					}
					else
					{
						$i->cropImage(ceil($width*$geo['height']/$height), $geo['height'], (($geo['width']-($width*$geo['height']/$height))/2), 0);
					}
			// thumbnail the image
					$i->ThumbnailImage($width,$height,true);
					$i->writeImages(FILE_ROOT_PATH . '/'.$newPath.'', true); // save or show or whatever the image
			//$i->setImageFormat("png");
			//header("Content-Type: image/png");
			//exit($i);	
		}
	}
	
		if(true===$opts['crop'])
		{
			$i->cropImage($opts['w'], $opts['h'], $opts['x'],$opts['y']);
			//Crop every other image
			$i->writeImages(FILE_ROOT_PATH. '/'.$newPath.'', true);//display image
	
		}
		if(true===$opts['scale'])
		{
			
			$i->resizeImage($opts['w'],$opts['h'],Imagick::FILTER_LANCZOS,1);
			$i->writeImages(FILE_ROOT_PATH . '/'.$newPath.'', true);//display image
		
		}
		else {
			
			$i->resizeImage($opts['w'],$opts['h'],Imagick::FILTER_LANCZOS,1);
			$i->writeImages(FILE_ROOT_PATH . '/'.$newPath.'', true);//display image
			
			
		}

	}
		
			return str_replace(FILE_ROOT_PATH,'',$newPath);

}


}

/* End of file Image_cropper.php */
/* Location: ./application/libraries/Image_cropper.php */